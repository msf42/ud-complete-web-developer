const basket = ['apples', 'oranges', 'grapes']

// all of these return the same thing
//1
for ( let i = 0; i < basket.length; i++) {
    console.log(basket[i]);
}

//2
basket.forEach(item => {
    console.log(item);
})

// for of
for (item of basket) {
    console.log(item);
}

// for in
const detailedBasket = {
    apples: 5,
    oranges: 10,
    grapes: 1000
}
    // properties of an object are enumerable
    // this is enumeration, not iteration
for (item in detailedBasket) {
    console.log(item);
}

// iterables = arrays, strings