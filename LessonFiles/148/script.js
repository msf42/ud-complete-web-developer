'Hellooooooo'.includes('o');
// true

const pets = ['cat', 'dog', 'bat']
pets.includes('dog');
// true
pets.includes('bird');
// false


const square = (x) => x**2
square(7);
// 49

const cube = (x) => x**3
cube(3);
// 27