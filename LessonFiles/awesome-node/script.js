const fs = require('fs')

fs.readFile('./hello.txt', (err, data) => {
  if (err) {
    console.log('errrrooooorrr');
  }
  console.log('Async', data.toString());
})

const file = fs.readFileSync('./hello.txt');
console.log('Sync', file.toString());

/* Output
Sync helloooooo there!!!!
Async helloooooo there!!!!
*/

// APPEND 
// fs.appendFile('./hello.txt', ' This is so cool!', err => {
//   if (err) {
//     console.log(err)
//   }
// });
/* hello.txt
helloooooo there!!!! This is so cool!
*/

// WRITE
fs.writeFile('bye.txt', 'Sad to see you go', err => {
  if (err) {
    console.log(err)
  }
})


// DELETE
fs.unlink('bye.txt', err => {
  if (err) {
    console.log(err)
  }
  console.log('Inception')
})