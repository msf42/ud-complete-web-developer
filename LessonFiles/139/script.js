const first = () => {
    const greet = 'Hi';
    const second = () => {
        alert(greet);
    }
    return second;
}

const newFunc = first();
newFunc();

//Closures - children always have access to parent scope

//Currying - converting functions that take multiple arguments
// into those that take args one at a time
const multiply = (a, b) => a * b;

const curriedMultiply = (a) => (b) => a * b;

// running in console
multiply(3,4)
12
curriedMultiply(3)(4)
12

// Compose - the output of two functions are used in a third
const compose = (f, g) => (a) => f(g(a));

const sum = (num) => num + 1;

compose(sum, sum)(5);  // returns 7

// Avoiding Side Effects, Functional Purity (always returning)
// *Deterministic = same input always produces same output