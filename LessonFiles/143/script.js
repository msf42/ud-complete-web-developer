// reference type
var object1 = { value: 10 };
var object2 = object1;
var object3 = { value: 10 };

object1 === object2

object1 === object3

// numbers, booleans, strings, etc are all primitive types
// reference types simply point to a value

/////////////////////////////////

// context vs scope
console.log(this); // your location - the object you are in

this.alert("hello"); // same as window.alert, because I am currently in the window

const object4 = {
    a: function() {
        console.log(this);
    }
} // now undefined

// instantiation
// when you make a copy of an object and reuse the code
class Player {
    constructor(name, type) {
        console.log('player', this);
        this.name = name;
        this.type = type;
    }
    introduce() {
        console.log(`Hi I am ${this.name}, I'm a ${this.type}`);
    }
}

class Wizard extends Player {
    constructor(name, type) {
        super(name, type);
        console.log('wizard', this);
    }
    play() {
        console.log(`WEEEE I'm a ${this.type}`);
    }
}

const wizard1 = new Wizard('Shelly', 'Healer');
const wizard2 = new Wizard('Shawn', 'Dark Magic');