function moveCommand(direction) {
    var whatHappens;
    switch (direction) {
        case "forward":
            whatHappens = "you encounter a monster"
            break;
        case "back":
            whatHappens = "you arrive home"
            break;
        case "right":
            whatHappens = "you found a river"
            break;
        case "left":
            whatHappens = "you run into a troll"
            break;
        default:
            whatHappens = "pleas eenter a valid direction"
    }
    return whatHappens;
}