1 == '1';
true
// javascript will convert to 1 == 1
// Type Coercion only works with '==';

1 === 1;
false
// === means explicit comparison


if (1) {
    console.log(5)
}
// output: 5; JS coerces 1 to 'true'