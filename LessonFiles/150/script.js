'Turtle'.padStart(10);
// "    Turtle"
'Turtle'.padEnd(10);
// "Turtle    "

//--------------------------------

const fun = (a,b,c,d,) => {
    console.log(a);
}

fun(1,2,3,4,)
// 1

// This is because it is often easier to write large functions
// like this:

const fun = (
            a,
            b,
            c,
            d,
            ) => {
                console.log(a);
            }

//---------------------------

Object.keys
Object.values
Object.entries

let obj = {
    username0: 'Santa',
    username1: 'Rudolph',
    username2: 'Grinch'
}

// old way
Object.keys(obj).forEach((key, index) => {
    console.log(key, obj[key]);
});

// username0 Santa
// username1 Rudolph
// username2 Grinch

// This is the new way
Object.values(obj).forEach(value => {
    console.log(value);
});
// Santa
// Rudolph
// Grinch


Object.values(obj).forEach(entries => {
    console.log(entries);
});
//[ 'username0', 'Santa' ]
//[ 'username1', 'Rudolph' ]
//[ 'username2', 'Grinch' ]

Object.entries(obj).map(value=> {
    return value[1] + value[0].replace('username', '');
})
// [ 'Santa0', 'Rudolph1', 'Grinch2' ]

