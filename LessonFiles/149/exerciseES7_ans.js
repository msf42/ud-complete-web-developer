// Solve the below problems:


// #1) Check if this array includes the name "John".
const dragons = ['Tim', 'Johnathan', 'Sandy', 'Sarah'];
dragons.includes('John')
// false

// #2) Check if this array includes any name that has "John" inside of it. If it does, return that
// name or names in an array.
const dragons = ['Tim', 'Johnathan', 'Sandy', 'Sarah'];
dragons.filter(name => name.includes('John'))

// #3) Create a function that calulates the power of 100 of a number entered as a parameter
const pow100 = (x) => x**100
pow100(2)
// 1.2676506002282294e+30

// #4) Using your function from #3, put in the paramter 10000. What is the result?
// Research for yourself why you get this result

// The result is 'infinity', because it essentially is.