var button = document.getElementById("enter");
var input = document.getElementById("userinput");
var ul = document.querySelector("ul");

function inputLength() {
	return input.value.length;
}


function createListElement() {
	var li = document.createElement("li");
	li.appendChild(document.createTextNode(input.value));
	ul.appendChild(li);
	
	// within the function that creates the list element
	// this will listen for a click on "this" element only
	li.addEventListener("click", function() {
		var finished = this.classList.toggle("done");
		// create a remove button
		var removeButton = document.createElement("button");
		// removeButton.classList.add("deleteButton");

		// if finished (clicked)
		if (finished) {
			// makes removeButton with 'remove' text
			removeButton.appendChild(document.createTextNode("remove"));
			// gives it the class "deleteButton", then adds it to list
			removeButton.classList = "deleteButton";
			li.appendChild(removeButton);

			// now if remove button is clicked
			removeButton.addEventListener("click", function() {
				this.parentElement.remove();
			});
		} else {
			this.getElementsByClassName("deleteButton")[0].remove();
		}
	})
	input.value = "";
}

function addListAfterClick() {
	if (inputLength() > 0) {
		createListElement();
	}
}

function addListAfterKeypress(event) {
	if (inputLength() > 0 && event.keyCode === 13) {
		createListElement();
	}
}

button.addEventListener("click", addListAfterClick);
input.addEventListener("keypress", addListAfterKeypress);