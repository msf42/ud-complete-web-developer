// Advanced Arrays
const array = [1, 2, 10, 16];

const double = []
const newArray = array.forEach((num) => {
    double.push(num * 2);
});

console.log('forEach', double);
// output
//(4) [2, 4, 20, 32]

// use map for simple loops. 
// Map always expects to return an element
// Map creates a new array
// Map reduces side effects

// map, filter, reduce
const mapArray = array.map((num) => {
    return num * 2
});

console.log('map', mapArray);
// output
//(4) [2, 4, 20, 32]

// can shorten to this if only one variable
const mapArray = array.map(num => num * 2);

// filter
const filterArray = array.filter(num => {
    return num > 5
});

console.log('filter', filterArray);

// output
// filter (2) [10, 16]


// reduce
const reduceArray = array.reduce((accumulator, num) => {
    return accumulator + num
}, 0);

console.log('reduce', reduceArray);
// output
// reduce 29