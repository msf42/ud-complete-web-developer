var a = 5;
var b = a;

b++;

console.log(a) // output = 5
console.log(b) // output = 6

let obj1 = {name: 'Yao', password:'123'};
let obj2 = obj1;

obj2.password = 'abc';

console.log(obj1); // output = abc
console.log(obj2); // output = abc


var c = [1, 2, 3, 4, 5];
var d = c;
d.push(13848480);
console.log(d);  // output = [1,2,3,4,5,13848480]
console.log(c);  // output = [1,2,3,4,5,13848480]

var e = [].concat(c);
c.push(888);
console.log(e); // output = [1,2,3,4,5,13848480]
console.log(c); // output = [1,2,3,4,5,13848480,888]



let obj = {a: 'a', b: 'b', c: 'c'};
let clone = Object.assign({}, obj);

obj.c = 5;
console.log(clone); // output: {a: "a", b: "b", c: "c"}

let clone2 ={...obj} // another way


let obj = {
    a: 'a',
    b: 'b',
    c: {
        deep: 'try and copy me'
    }
};
let clone = Object.assign({}, obj);
let clone2 = {...obj};

obj.c.deep = 'hahaha';
console.log(obj); 
console.log(clone);
console.log(clone2);
// output of all 3 is
// {a: 'a', b: 'b', c: {deep: 'try and copy me'}}
// this is a 'shallow clone'
// the second address never changed


let superClone = JSON.parse(JSON.stringify(obj));
obj.c.deep = 'changed!';
console.log(obj); // still 'changed!'
console.log(superClone); // still 'hahaha'