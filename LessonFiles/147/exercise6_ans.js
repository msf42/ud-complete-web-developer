//Evaluate these:
//#1
[2] === [2] // false
{} === {} // false

//#2 what is the value of property a for each object.
const object1 = { a: 5 }; 
const object2 = object1; 
const object3 = object2; 
const object4 = { a: 5}; 
object1.a = 4; // 4, 4, 4, 5


//#3 create two classes: an Animal class and a Mammal class. 
// create a cow that accepts a name, type and color and has a sound method that moo's her name, type and color. 

class Animal {
    constructor(name, type) {
        console.log('Animal', this);
        this.name = name;
        this.type = type;
    }
    introduce() {
        console.log(`Hi, I am a ${this.name}, and I am a ${this.type}`);
    }
}

const Animal1 = new Animal('Daisy', 'cow');
Animal1.introduce(); //Hi, I am a Daisy, and I am a cow


class Cow extends Animal {
    constructor(name, type, color, sound) {
        super(name, type);
        this.sound = sound;
        this.color = color;
        console.log('Cow', this);
    }
    intro2() {
        console.log(`Hi, I am ${this.name} the ${this.color} ${this.type} and I say ${this.sound}`);
    }
}

const Cow1 = new Cow('Daisy', 'dairy cow', 'brown', 'moo' );
Cow1.intro2(); //Hi, I am Daisy the brown dairy cow and I say moo
