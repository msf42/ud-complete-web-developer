# Complete Web Developer - Zero to Mastery

These are my course notes for [Complete Web Developer - Zero to Mastery](https://www.udemy.com/course/the-complete-web-developer-zero-to-mastery/) on [Udemy](www.udemy.com).

- I have taken several courses from  Andrei Neagoie and I highly recommend them.
- These are the notes I took for personal use as I needed. They are not polished and may be difficult to follow at times, but I post my notes online for a couple of reasons:
  - to keep myself motivated and hold myself accountable
  - in hopes that it may help someone else taking the same course

![Completion Certificate](./CompleteWebDev.jpg)
