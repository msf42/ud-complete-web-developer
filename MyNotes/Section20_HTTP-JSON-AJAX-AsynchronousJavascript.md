# Section 20: HTTP/JSON/AJAX + Asynchronous Javascript

## 228: HTTP/HTTPS

Only 4 words of HTTP:

- GET
- POST
- PUT
- DELETE

![image](./images/s_ED4B24C704A4BC73C5B957FBCAA71F1FFACA2F0655398EE08E34D9A1E1B4F886_1561074378099_image.png)

Checking Developer Tools - Network, using example.com is helpful at seeing how HTTP works

---

## 229: JSON

### JSON = JavaScript Object Notation

- A syntax for storing and exchanging data

### JSON and XML do the same thing

![image](./images/s_ED4B24C704A4BC73C5B957FBCAA71F1FFACA2F0655398EE08E34D9A1E1B4F886_1561075389412_image.png)

JSON is basically a universal language for the internet

---

## 230: JSON vs Form Data

Originally, the only way to submit some form data to a server was through the `<form>` tag in HTML. As we have learned, it can do a POST or a GET request. With JSON you can now grab the contents of the `<input>` in a form and submit those with JSON instead of as a form data. You can now submit to the server whenever you want without it necessarily being a `<form>`, through AJAX.. What is AJAX you might say?

It's coming up in the next video!

---

## 231: AJAX

This prevents an entire webpage from reloading every time something changes.

![The Old Way: XHR](./images/s_ED4B24C704A4BC73C5B957FBCAA71F1FFACA2F0655398EE08E34D9A1E1B4F886_1561497285966_image.png)
![The New Old Way: jQuery](./images/s_ED4B24C704A4BC73C5B957FBCAA71F1FFACA2F0655398EE08E34D9A1E1B4F886_1561497322477_image.png)
![The New Way: Fetch](./images/s_ED4B24C704A4BC73C5B957FBCAA71F1FFACA2F0655398EE08E34D9A1E1B4F886_1561497347145_image.png)

![image](./images/s_ED4B24C704A4BC73C5B957FBCAA71F1FFACA2F0655398EE08E34D9A1E1B4F886_1561497461340_image.png)

---

## 232: Promises

A ***Promise*** is an object that may produce a single value sometime in the future.

- either a resolved value, or a reason that it was not resolved
  - fulfilled, rejected, or pending
  
```js
const promise = new Promise((resolve, reject) => {
  if (true) {
    resolve('Stuff worked');
  } else {
    reject('Error, it broke');
  }
})

const promise2 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'HIII')
})

const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 1000, 'POOKIE')
})

const promise4 = new Promise((resolve, reject) => {
  setTimeout(resolve, 3000, 'Is it me you are looking for')
})

Promise.all([promise, promise2, promise3, promise4])
  .then(values => {
    console.log(values);
  })

// results
  // 4) ["Stuff worked", "HIII", "POOKIE", "Is it me you are looking for"]
  // 0: "Stuff worked"
  // 1: "HIII"
  // 2: "POOKIE"
  // 3: "Is it me you are looking for"
  // length: 4

promise
  .then(result => result + "!")
  .then(result2 => result2 + '?')
  .catch(() => console.log('errrror!'))
  .then(result3 => {
    console.log(result3 + '!');
  }) // returns Stuff worked!?!

  /////////////////////////////////////////
  const urls = [
    'https://jsonplaceholder.typicode.com/users',
    'https://jsonplaceholder.typicode.com/posts',
    'https://jsonplaceholder.typicode.com/albums'
  ]

  Promise.all(urls.map(url => {
    return fetch(url).then(resp=> resp.json())
  })).then(results => {
    console.log(results[0]) // returns users
    console.log(results[1]) // returns posts
    console.log(results[2]) // returns albums
  }).catch(() =>.console.log('error'))
```

---

## 233: Exercise: Promises

```js
// Solve the questions below:
// #1) Create a promise that resolves in 4 seconds and returns "success" string

const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("success");
  }, 4000)
});
// #2) Run the above promise and make it console.log "success"
const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(console.log("success"));
  }, 4000)
});
// #3) Read about Promise.resolve() and Promise.reject(). How can you make
// the above promise shorter with Promise.resolve() and console loggin "success"
const promise = Promise.resolve( // goes straight to 'resolve' step
  setTimeout(() => {
    console.log("success");
  }, 4000)
);

// #4) Catch this error and console log 'Ooops something went wrong'
Promise.reject('failed')
  .catch(console.log('Ooops something went wrong'))
// #5) Use Promise.all to fetch all of these people from Star Wars (SWAPI) at the same time.
// Console.log the output and make sure it has a catch block as well.
const urls = [
  'https://swapi.co/api/people/1',
  'https://swapi.co/api/people/2',
  'https://swapi.co/api/people/3',
  'https://swapi.co/api/people/4'
]
Promise.all(urls.map(url =>
  fetch(url).then(people => people.json())
))
.then(array => {
  console.log('1', array[0])
  console.log('2', array[1])
  console.log('3', array[2])
  console.log('4', array[3])
})
.catch(err => console.log('ughhhh fix it!', err));
```

---

## 234: ES8 - Async Await

This is another way of writing promises. It does the same thing, but makes it look cleaner.

## Some before and after examples:

![image](./images/s_ED4B24C704A4BC73C5B957FBCAA71F1FFACA2F0655398EE08E34D9A1E1B4F886_1561727433271_image.png)

![image](./images/s_ED4B24C704A4BC73C5B957FBCAA71F1FFACA2F0655398EE08E34D9A1E1B4F886_1561727971038_image.png)

![image](./images/s_ED4B24C704A4BC73C5B957FBCAA71F1FFACA2F0655398EE08E34D9A1E1B4F886_1561728292430_image.png)

---

## 235: Exercise: ES8 - Async Await

```js
// Solve the below problems:
// #1) Convert the below promise into async await
fetch('https://swapi.co/api/starships/9/')
  .then(response => response.json())
  .then(console.log)
async function getStarships() {
  const resp = await fetch('https://swapi.co/api/starships/9/')
  const data = await resp.json();
  console.log(data);
}

// #2) ADVANCED: Update the function below from the video to also have
// async await for this line: fetch(url).then(resp => resp.json())
// So there shouldn't be any .then() calls anymore!
// Don't get discouraged... this is a really tough one...
const urls = [
  'https://jsonplaceholder.typicode.com/users',
  'https://jsonplaceholder.typicode.com/posts',
  'https://jsonplaceholder.typicode.com/albums'
]
const getData = async function() {
  const [ users, posts, albums ] = await Promise.all(urls.map(url =>
      fetch(url).then(resp => resp.json())
  ));
  console.log('users', users);
  console.log('posta', posts);
  console.log('albums', albums);
}
const getData = async function() {
  const [ users, posts, albums ] = await Promise.all(urls.map(async function(url) {
      const response = await fetch(url);
      return response.json();
  }));
  console.log('users', users);
  console.log('posta', posts);
  console.log('albums', albums);
}

// #3) Add a try catch block to the #2 solution in order to catch any errors.
// Now chnage one of the urls so you console.log your error with 'ooooooops'
const getData = async function() {
  try {
    const [ users, posts, albums ] = await Promise.all(urls.map(async function(url) {
      const response = await fetch(url);
      return response.json();
    }));
    console.log('users', users);
    console.log('posta', posts);
    console.log('albums', albums);
  } catch (err) {
    console.log('oooooops', err)
  }
}
```

---

## 236: ES9 (2018)

```js
const animals = {
  tiger: 23,
  lion: 5,
  monkey: 2,
  bird: 40
}
function objectSpread(p1, p2, p3) {
  console.log(p1)
  console.log(p2)
  console.log(p3)
}
const { tiger, lion, ...rest } = animals;
objectSpread(tiger, lion, rest);
```

---

## 237: ES9 (2018) - Async

```js
restParam(1, 2, 3, 4, 5);
function restParam(p1, p2, ...p3) {
  // p1 = 1
  // p2 = 2
  // p3 = [3, 4, 5]
}

const myObject = {
  a: 1,
  b: 2,
  c: 3
};
const { a, ...x } = myObject;
// a = 1
// x = { b: 2, c: 3 }
```

```js
//finally
const urls = [
  'https://swapi.co/api/people/1',
  'https://swapi.co/api/people/2',
  'https://swapi.co/api/people/3',
  'https://swapi.co/api/people/4'
]
Promise.all(urls.map(url => fetch(url).then(people => people.json())))
  .then(array => {
    throw Error
    console.log('1', array[0])
    console.log('2', array[1])
    console.log('3', array[2])
    console.log('4', array[3])
  })
  .catch(err => console.log('ughhhh fix it!', err))
  .finally(() => console.log('extra action here'))
    const urls = [
      'https://jsonplaceholder.typicode.com/users',
      'https://jsonplaceholder.typicode.com/postss',
      'https://jsonplaceholder.typicode.com/albums'
    ]
    const getData = async function() {
      try {
        const [ users, posts, albums ] = await Promise.all(urls.map(async function(url) {
            const response = await fetch(url);
            return response.json();
        }));
        console.log('users', users);
        console.log('posts', posts);
        console.log('albums', albums);
      } catch (err) {
        console.log('ooooooops', err);
      }
    }
    //New for await of feature:
    const loopThroughUrl = (urls) => {
      for (url of urls) {
        console.log(url)
      }
    }

    const getData2 = async function() {
      const arrayOfPromises = urls.map(url => fetch(url));
      for (const request of arrayOfPromises) {
        console.log(request);
      }
      for await (const request of arrayOfPromises) {
        const data = await request.json();
        console.log(data)
      }
      //In this case, for-await takes each item from the array and waits for it to resolve. You'll get the first response even if the second response isn't ready yet, but you'll always get the responses in the correct order.
    }
```

---

## 238: Reviewing ES6, ES7, ES8, ES9, ES10 Features

[New Features](https://github.com/daumann/ECMAScript-new-features-list)

---
