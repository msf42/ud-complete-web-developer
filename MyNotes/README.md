# Notes for Udemy Complete Web Developer - Zero to Mastery Course

- [Section 01 - 05](Section01-05.md)
    -   01 - Introduction
    - 02 - How the Internet Works
    - 03 - History of the Web
    - 04 - HTML5
    - 05 - Advanced HTML 5

- [Section 06 - 08](Section06-08.md)
    - 06 - CSS
    - 07 - Advanced CSS
    - 08 - Bootstrap 4, Templates, & Building Your Startup Landing Page

- [Section 09 - 10](Section09-10.md)
    - 09 - CSS Grid + CSS Layout
    - 10 - Career of a Web Developer

- [Section 11: JavaScript](Section11_Javascript.md)

- [Section 12: DOM Manipulation](Section12_DOMManipulation.md)

- [Section 13: Advanced JavaScript](Section13_AdvancedJavascript.md)

- [Section 16: Git, Github, and Open Source Projects](Section16_GitGithubOpenSourceProjects.md)

- [Section 17: A Day in the Life of a Developer](Section17_ADayInTheLifeOfADeveloper.md)

- [Section 18: NPM & NPM Scripts](Section18_NPM-NPMScripts.md)

- [Section 19: React.js and Redux](Section19_ReactjsRedux.md)

- [Section 20: HTTP/JSON/AJAX + Asynchronous Javascript](Section20_HTTP-JSON-AJAX-AsynchronousJavascript.md)

- [Section 21: Backend Basics](Section21_BackendBasics.md)

- [Section 22: APIs](Section22_APIs.md)

- [Section 23: FINAL PROJECT: SmartBrain FrontEnd](Section23_FINAL_PROJECTSmartBrainFrontEnd.md)

- [Section 24: Node.js & Express.js](Section24_NodeJsAndExpresJs.md)

- [Section 25: Final Project: SmartBrain Back-End Server](Section25_FINAL_PROJECTSmartBrainBackendServer.md)

- [Section 26: Databases](Section26_Databases.md)

- [Section 27: FINAL PROJECT: SmartBrain Back-End Database](Section27_FINAL_PROJECT%253aSmartBrainBackEndDatabase.md)

---
