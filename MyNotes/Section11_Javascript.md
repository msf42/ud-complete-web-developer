# Section 11: Javascript

## Lesson 100: Introduction to Javascript

Just basic welcome.

---

## Lesson 101: What is Javascript?

Basic history of Javascript

- Created by Netscape in 1995
- A way to add actions to websites - Javascript is the ‘verb’ in websites.

---

## Lesson 102: Your First Javascript

### Javascript Types

#### Number

```js
3 + 4
7
4 + 5
9
3 * 5
15
12/4
3
[3+4]*2
14
12 % 5
2
```

#### String

```js
"Hello" + " There"
"Hello There"

// Quotes work the same as python. Use double quotes when an apostrophe is needed. Also can use \backslash

10 + "34"
"1034"
// Javascript will auto convert the num to str

10 - "3"
7
// Javascript auto converts the str to num

"hello" - "bye"
```

#### NaN

```js
NaN // = Not a Number
```

#### Boolean

```js
true
false
5 > 10
false

5 >= 5
true

5 < 5
false

// Javascript uses two different types of =
// === must strictly equal each other
// == will adjust for type, as below
3 == "3"
true

3 === "3"
false
```

---

## Lesson 103: Number, String, Boolean Comparisons

```js
5 + "34"
"534"

5 - "4"
1

10 % 5
0

5 % 10
5

"Java" + "Script"
"JavaScript"

" " + " "
"  "

" " + 0
" 0"

true + true
2

true + false
1

false + true
1

false - true
-1

3 - 4
-1

"Bob" - "bill"
NaN

5 >= 1
true

0 === 1
false

4 <= 1
false

1 != 1
false

"A" > "B"
false

"B" < "C"
true

"a" > "A"
true

"b" < "A"
false

true === false
false

true != false
true

"Hi There!" + " It's" + ' "sunny" out'
"Hi There! It's "sunny" out"
```

---

## Lesson 104: Variables

```js
var george = "These pretzels are making me thirsty" + "!";

george
"These pretzels are making me thirsty!"

var three = 3

three
3

// must start with a letter, $, or _

prompt()
"Hello"
// This gives a pop-up box, and returns what you type

prompt("What is your username?")
"msf42"

var first = prompt("Enter first number");
var second = prompt("Enter second number");
// pop-up asked for these, now they are stored

first
"3"

second
"5"

var sum = Number(first) + Number(second)

sum
8

alert("hi there!!!")
// just a pop-up with OK button
```

### Now for our first program

```js
var first = prompt("Enter first number");
var second = prompt("Enter second number");
var sum = Number(first) + Number(second);
alert("The sum is: " + sum);

// variables can be reassigned, just like python
// undefined is used when nothing is assigned to a variable'

var b

b
undefined
```

---

## Lesson 105: Variables Exercise

### Instructions

```js
// Evaluate what answers you would get if you ran this in the
// Javascript Console in Google Chrome. Answer the questions then
// check them by copying them and running it in the console yourself
// line by line

// add variable "firstName" and "lastName" // so that they equal your name
var firstname = "Steve"
undefined
var lastname = "Furches"
undefined
firstname + " " + lastname
"Steve Furches"

// create a variable that holds the answer // of "firstName" + " " + "lastName"
var x = firstname + " " + lastname
undefined
x
"Steve Furches"

// Evaluate this question. What is a + b?
var a = 34;
var b = 21;
a = 2;
a + b // what is the answer here?
var a = 34;
var b = 21;
a = 2;
2
a + b
23

// What is c equal to?
var c;
var c;
undefined
```

---

## Lesson 106: Exercise: Make a Calculator

```js
// Make a Calculator! using prompt(), and variables, make a program that does the following:
// 1. Prompts the user for first number.
// 2. Stores that first number
// 3. Prompts the user for the second number.
// 4. stores that number and responds with the SUM by using an alert.  
var first = prompt("Enter first number");
var second = prompt("Enter second number");
var sum = Number(first) + Number(second);
alert("The sum is: " + sum);

// BONUS: Make a program that can subtract, multiply, and also divide!
var first = prompt("Enter first number");
var second = prompt("Enter second number");
var sum = Number(first) + Number(second);
var diff = Number(first) - Number(second);
var prod = Number(first) * Number(second);
var quot = Number(first) / Number(second);
alert("The sum is: " + sum);
alert("The difference is: " + diff);
alert("The product is: " + prod);
alert("The quotient is: " + quot);
```

---

## Lesson 107: Control Flow

All pretty straightforward stuff, similar to python, but with slightly different syntax

```js
var name = "Billy"
if (name === "Billy") {
    alert("hi Billy!");
}

name = "Susy";
"Susy"
if (name === "Billy") {
    alert("hi Billy!");
}

if (name === "Billy") {
    alert("hi Billy!");
} else {
        alert("hmm I don't know you");
}

// this is an if-else statement

if (name === "Billy") {
    alert("hi Billy!");
} else if (name=== "Susy") {
        alert("hi Susy");
} else {
    alert("I don't know you");
}
```

```js
// logical operators
if (name === "Billy" || name === "Ann") {
    alert("Hi Billy or Ann!");
}

name = "Billy"
"Billy"
if (name === "Billy" || name === "Ann") {
    alert("Hi Billy or Ann!");
}

if (name === "Billy" && name === "Ann") {
    alert("Hi Billy or Ann!");
}

name = "Ann"
"Ann"
if (name === "Billy" && name === "Ann") {
    alert("Hi Billy or Ann!");
}

// nothing happened. name can't equal both

if (firstname === "Bob" && lastname === "Smith") {
    alert"Hi Bob Smith!");
VM2031:2 Uncaught SyntaxError: Unexpected string
if (firstname === "Bob" && lastname === "Smith") {
    alert("Hi Bob Smith!");
}

firstname = "Bob"
"Bob"
lastname = "Smith"
"Smith"
if (firstname === "Bob" && lastname === "Smith") {
    alert("Hi Bob Smith!");
}
```

### `!` negates, just like python

```js
if (!(name === "Bob")) {
    alert("Hi Bob");
}
undefined
name
"Ann"
// ! means does not equal, similar to python
undefined
!false
true
!true
false
```

---

## Lesson 108: Make a Keyless Car

```js
var age = prompt("How old are you?");
if (age < 18) {
    alert("Sorry, you are too young to drive this car. Powering off");
} else if (age == 18) {
        alert("Congratulations on your first year of driving. Enjoy the ride!");
} else if (age > 18) {
    alert("Powering On. Enjoy the ride!");
} else {
    alert("Age must be a number!");
}
```

---

## Lesson 109: Javascript on Our Webpage

### index.html

```html
<!DOCTYPE html>
<html>
<head>
        <title>Javascript</title>
        <link rel="stylesheet" type="text/css"
        href="">
</head>
<body>
        <h1>Javascript in HTML</h1>
        <script type="text/Javascript" src="script.js">
        </script>
</body>
</html>
```

### script.js

```js
4 + 3

if (4+3 === 7) {
        alert("You're smart");
        console.log("Hellooooo"); // this is saved to the console log
}
```

---

## Lesson 110: Functions

```js
alert()
undefined

prompt()
""

alert
ƒ alert() { [native code] }

alert("Hi there!")
undefined
```

### Function Declaration and Function Expression

```js
// Function Declaration

function sayHello() {
        console.log("Hello");
}

sayHello();

// Function Expression

var sayBye = function() {  // this is an anonymous function
        console.log("Bye");
}

sayBye();  
```

```js
function sing() {
    console.log("AHHHHHH");
    console.log("TEEEEEE");
}

sing();

// Now if I want to change lyrics, I would have to rewrite it, or....

function sing(song) {
    console.log(song);
}

sing("Laaa deee daaa");
sing("Hellooooooo");
sing("backstreets back alright");
```

```js
function multiply(a, b) {
        if (a > 10 || b > 10) {
                return "that's too hard";
        } else {
                return a * b;
        }
}
// as soon as we say 'return', the function is done
```

```js
multiply(5,10);
50

multiply(55, 11);
"that's too hard"
```

```js
function multiply(a, b) {
        return a * b;
}

alert(multiply(3,4));
```

---

## Lesson 111: Make a Keyless Care Even Better

### Question

```js
// Make a keyless car EVEN BETTER!
// We are improving our car from previous exercise now.
// Solutions to future exercises will be in the .js file (meaning Javascript file) from now on.

var age = prompt("What is your age?");

if (Number(age) < 18) {
        alert("Sorry, you are too yound to drive this car. Powering off");
} else if (Number(age) > 18) {
        alert("Powering On. Enjoy the ride!");
} else if (Number(age) === 18) {
        alert("Congratulations on your first year of driving. Enjoy the ride!");
}

//1. Make the above code have a function called checkDriverAge(). Whenever you call this function, you will get prompted for age. Use Function Declaration to create this function.
// Notice the benefit in having checkDriverAge() instead of copying and pasting the function everytime?

//2. Create another function that does the same thing, called checkDriverAge2() using Function Expression.

//BONUS: Instead of using the prompt. Now, only use console.log and make the checkDriverAge() function accept an argument of age, so that if you enter:
checkDriverAge(92);
it returns "Powering On. Enjoy the ride!"
```

### Answer

```js
function checkDriverAge() {
        var age = prompt("What is your age?");
        if (Number(age) < 18) {
                alert("Sorry, you are too yound to drive this car. Powering off");
        } else if (Number(age) > 18) {
                alert("Powering On. Enjoy the ride!");
        } else if (Number(age) === 18) {
                alert("Congratulations on your first year of driving. Enjoy the ride!");
        }
}

var checkDriverAge2 = function() {
        var age = prompt("What is your age?");
        if (Number(age) < 18) {
                alert("Sorry, you are too yound to drive this car. Powering off");
        } else if (Number(age) > 18) {
                alert("Powering On. Enjoy the ride!");
        } else if (Number(age) === 18) {
                alert("Congratulations on your first year of driving. Enjoy the ride!");
        }
}
```

---

## Lesson 112: Data Structures: Arrays

### script.js

```js
var list = ["tiger", "cat", "bear", "bird"]; // could be called anything
console.log(list[1]); // returns 'cat'
```

### output

```js
cat
```

### Working in browser

```js
cat
var list = ["tiger", "cat", "bear", "bird"];
undefined
list
    1. (4) ["tiger", "cat", "bear", "bird"]
      1. 0: "tiger"
      2. 1: "cat"
      3. 2: "bear"
      4. 3: "bird"
      5. length: 4
      6. __proto__: Array(0)
var numbers = [1,2,3,4]
undefined
var booleans = [true, false, true];
undefined
var functionList = [function apple() {
    console.log("apple");
}]
undefined
functionList
    1. [ƒ]
      1. 0: ƒ apple()
      2. length: 1
      3. __proto__: Array(0)
var mixedList = ["apples", 3, undefined, true, function apple() {
    console.log("apples")
}];
undefined
mixedList
    1. (5) ["apples", 3, undefined, true, ƒ]
      1. 0: "apples"
      2. 1: 3
      3. 2: undefined
      4. 3: true
      5. 4: ƒ apple()
      6. length: 5
      7. __proto__: Array(0)


    var list = [
            ["tiger", "cat", "bear", "bird"]
    ];

 console.log(list\[0\][2]); // this would return bear


    var list = ["tiger", "cat", "bear", "bird"];
    undefined
    list.shift();
    "tiger"
    list
        1. (3) ["cat", "bear", "bird"]
    list.pop();
    "bird"
    list
        1. (2) ["cat", "bear"]
    list.push("elephant");
    3
    list
        1. (3) ["cat", "bear", "elephant"]
    list.concat(["bee", "deer"]);
        1. (5) ["cat", "bear", "elephant", "bee", "deer"]
          1. 0: "cat"
          2. 1: "bear"
          3. 2: "elephant"
          4. 3: "bee"
          5. 4: "deer"
          6. length: 5
          7. __proto__: Array(0)
    list.sort();
        1. (3) ["bear", "cat", "elephant"]
    var myList = ["cat", "bear", "elephant", "bee", "deer"]
    undefined
    var myNewList = myList.concat(["monkey"]);
    undefined
    myList
        1. (5) ["cat", "bear", "elephant", "bee", "deer"]
    myNewList
        1. (6) ["cat", "bear", "elephant", "bee", "deer", "monkey"]
```

---

## Lesson 113: Exercise: Arrays

```js
    // using this array,
    // var array = ["Banana", "Apples", "Oranges", "Blueberries"];

// 1. Remove the Banana from the array.
// 2. Sort the array in order.
```

### Answers

```js
    var array = ["Banana", "Apples", "Oranges", "Blueberries"];
    undefined
    array.shift();
    "Banana"
    array
        1. (3) ["Apples", "Oranges", "Blueberries"]
    array.sort();
        1. (3) ["Apples", "Blueberries", "Oranges"]
    array.push("Kiwi");
    4
    array
        1. (4) ["Apples", "Blueberries", "Oranges", "Kiwi"]
    array.splice(0,1);
        1. ["Apples"]
    array
        1. (3) ["Blueberries", "Oranges", "Kiwi"]
    array.reverse();
        1. (3) ["Kiwi", "Oranges", "Blueberries"]
    var array2 = ["Banana", ["Apples", ["Oranges"], "Blueberries"]];
    undefined
    array2\[1\][1][0];
    "Oranges"
```

---

## Lesson 114: Data Structures: Objects

### Objects are collections of properties

### script.js

```js
var user = {
        name: "John",
        age: 34,
        hobby: "Soccer",
        isMarried: false,
};

var list = ["apple", "banana", "orange"];
```

### browser

```js
user
    1. {name: "John", age: 34, hobby: "Soccer", isMarried: false}
      1. age: 34
      2. hobby: "Soccer"
      3. isMarried: false
      4. name: "John"
      5. __proto__: Object

list
    1. (3) ["apple", "banana", "orange"]
      1. 0: "apple"
      2. 1: "banana"
      3. 2: "orange"
      4. length: 3
      5. __proto__: Array(0)

list[1]
"banana"

user.name
"John"

user.age
34

user.hobby
"Soccer"

user.isMarried
false

user.favoriteFood = "spinach";
"spinach"

user
    1. {name: "John", age: 34, hobby: "Soccer", isMarried: false, favoriteFood: "spinach"}
      1. age: 34
      2. favoriteFood: "spinach"
      3. hobby: "Soccer"
      4. isMarried: false
      5. name: "John"
      6. __proto__: Object

user.isMarried = true
true
```

**Why isn’t an array a js type, but an object is?**
Ultimately, an array is just a list, but an object can carry complex things, like user information.
Example:

- In a game
  - a character might be an object. This object carries all of that user’s characteristics
  - the spells might be a list

You can have an array inside an object.

### script.js

```js
var user = {
        name: "John",
        age: 34,
        hobby: "Soccer",
        isMarried: false,
        spells: ["abrakadabra", "shazam", "boo"],
        shout: function() {
                console.log("AHHHHHHHHH");
        }
};

var list = [
        {
                username: "andy",
                password: "secret"
        },
        {
                username: "jess",
                password: "123"
        }
];
```

### browser

```js
list
    1. (2) [{…}, {…}]
      1. 0: {username: "andy", password: "secret"}
      2. 1: {username: "jess", password: "123"}
      3. length: 2
      4. __proto__: Array(0)

user.spells
    1. (3) ["abrakadabra", "shazam", "boo"]
      1. 0: "abrakadabra"
      2. 1: "shazam"
      3. 2: "boo"
      4. length: 3
      5. __proto__: Array(0)

user.spells[1];
"shazam"

list[0].password;
"secret"

user.shout()  // this is called a method
script.js:8 AHHHHHHHHH
```

- A function inside an object is a **method** (like `user.shout()` above
- `list.pop()` and `console.log` are too

```js
user2={}
    1. {}

user2
    1. {}

list2=[];
    1. []

list2
    1. []

list2[0]
undefined

var emptyObject = {}
undefined

emptyObject
    1. {}

var nullObject = null;
undefined

nullObject
null

nullObject.name = "Annie"
VM752:1 Uncaught TypeError: Cannot set property 'name' of null
    at <anonymous>:1:17

emptyObject.name = "Annie";
"Annie"
```

Empty objects can be added to, but null objects can not. Null objects may seem useless, but there will be more discussion later about them and their uses.

---

## Lesson 115: Objects and Arrays for Facebook App

```js
// Create an object and an array which we will use in our facebook exercise.

// 1. Create an object that has properties "username" and "password". Fill those values in with strings.

// 2. Create an array which contains the object you have made above and name the array "database".

// 3. Create an array called "newsfeed" which contains 3 objects with properties "username" and "timeline".
```

- Answer included in next lesson.

---

## Lesson 116: Exercise: Build Facebook

```js
// user database
var database = [
        {
                username: "steve",
                password: "annie42"
        }
];

// newsfeed
var newsFeed = [
        {
                username: "Annie",
                timeline: "Hello World"
        },
        {
                username: "Leo",
                timeline: "Whwhaapwhhhiiiize!"
        },
        {
                username: "Zack",
                timeline: "I'm a grumpy teenager."
        }
];

// prompt to get name and pass
var userNamePrompt = prompt("What is your username?");
var passwordPrompt = prompt("What is your password?");

// check if name and pass are correct
function signIn(user, pass) {
        if (user === database[0].username &&
                pass === database[0].password) {
                console.log(newsFeed);
        } else {
                alert("Sorry, wrong username and password!");
        }
}

// run the function, insert data that user entered into function
signIn(userNamePrompt,passwordPrompt);
```

---

## Lesson 117: Javascript Terminology

### function declaration

```js
function newFunction() {

}
```

### function expression

```js
// can have name, but usually anonymous
var newFunction = function() {

}; // notice semicolon

// expression
1 + 3;
var a = 2;
return true;

// calling or invoking a function
alert();
newFunction(param1,param2);

// assign a variable
var a = true;

// function vs method
function thisIsAFunction() {

}

var obj = {
        thisIsAMethod: function() {

        }
}

thisIsAFunction();
obj.thisIsAMethod();
```

---

## Lesson 118: Loops

### For Loops

```js
var todos = [
        "clean room",
        "brush teeth",
        "exercise",
        "study javascript",
        "eat healthy"
]

// i is just random variable
// as long as i < the length of the todos list
// i++ means i+1
for (var i=0; i < todos.length; i++) {
        console.log(todos[i] + "!");
}

// this will do the same as above
for (var i=0; i < todos.length; i++) {
        todos[i] = todos[i] + "!";
}

 // this will delete them all
    var todosLength = todos.length;
    for (var i=0; i < todosLength; i++) {
            todos.pop();
    }
```

### While Loops

```js
var todosLength = todos.length;

var counterOne = 10;
while (counterOne > 0) {
        console.log(counterOne);
        counterOne--
}
```

### Do Loops

```js
var counterTwo = 10
do {
    console.log(counterTwo);
    counterTwo--;
} while (counterTwo > 0);
```

`While` loops and `do while` loops look about the same, however there is a key difference:

- the `while` loop checks the condition first, then performs the action
- the `do while` loop performs the action first, then checks the condition

Ultimately, `for` loops are the most common and most useful

### forEach loops

```js
var todos = [
    "clean room",
    "brush teeth",
    "exercise",
    "study javascript",
    "eat healthy"
];
var todosLength = todos.length;

// here is a regular for loop

for (var i=0; i < todosLength; i++) {
    console.log(todos[i], i);
}

// here is a forEach loop that does the same thing
todos.forEach(function(todo, i) {
    console.log(todo, i);
})

// they both print this:
clean room 0
brush teeth 1
exercise 2
study javascript 3
eat healthy 4

// here is another way
function logTodos(todo, i) {
    console.log(todo, i);
}

todos.forEach(logTodos);  // could use same function later on,
                          // such as todosImportant.forEach(logTodos);
```

---

## Lesson 119: Build Facebook 2

```js
var database = [
    {
        username: "steve",
        password: "annie42"
    },
    {
        username:"sally",
        password:"123"
    },
    {
        username:"ingrid",
        passwrod:"777"
    }
];
var newsFeed = [
    {
        username: "Annie",
        timeline: "Hello World"
    },
    {
        username: "Leo",
        timeline: "Whwhaapwhhhiiiize!"
    },
    {
        username: "Zack",
        timeline: "I'm a grumpy teenager."
    }
];

// this checks and returns true/false for if user is valid
function isUserValid(user, pass) {
    for (var i=0; i < database.length; i++)  {
        if (database[i].username === user &&
            database[i].password === pass)  {
            return true;
        }
    }
    return false;
}

// if the user is valid, return newsfeed, if not, return message
function signIn(user, pass) {
    if  (isUserValid(user, pass)) {
        console.log(newsFeed);
    } else {
        alert("Sorry, wrong username and password!");
    }
}
var userNamePrompt = prompt("What is your username?");
var passwordPrompt = prompt("What is your password?");
signIn(userNamePrompt,passwordPrompt);
```

---

## Lesson 120: Javascript Keywords

Just a brief review of Keywords

---

## Lesson 121: Section Outline Document

```text
JAVASCRIPT TYPES
----------
1. Number
2. String
3. Boolean
4. Undefined
5. Null
<!-- 6. Symbol (new in ECMAScript 6) -->
7. Object

JAVASCRIPT COMPARISONS
----------

!==
===
>=
<=
>
<

JAVASCRIPT VARIABLES
----------
var
<!-- let (new in ECMAScript 6)-->  
<!-- const (new in ECMAScript 6)-->

JAVASCRIPT CONDITIONALS
----------
if
else
else if
<!-- ternary operator -->
<!-- switch -->

JAVASCRIPT LOGICAL OPERATORS
----------
&&
||
!

JAVASCRIPT FUNCTIONS
----------
var a = function name() {}
function name() {}
return
<!-- () => (new in ECMAScript 6) -->

JAVASCRIPT DATA STRUCTURES
----------
Array
Object

JAVASCRIPT LOOPING
----------
for
while
do
forEach (new in ECMAScript 5)

JAVASCRIPT KEYWORDS
----------
break
case
catch
class
const
continue
debugger
default
delete
do
else
export
extends
finally
for
function
if
import
in
instanceof
new
return
super
switch
this
throw
try
typeof
var
void
while
with
yield
```

---
