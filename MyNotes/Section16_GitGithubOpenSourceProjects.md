# Section 16: Git + Github + Open Source Projects

## 174: Installing Git

Already did this in Git course

---

## 175: Git & Github Part 1

Basic Github commands that I learned previously.

---

## 176: Git & Github Part 2

[Git Cheat Sheet](https://education.github.com/git-cheat-sheet-education.pdf)

---

## 177: My Terminal Setup

Done.

---

## 178: Contributing to Open Source

Great lesson. Forked 2 repositories on Github. Made 2 pull requests.

---

## 179: Contribute to Open Source 2

More.

---

## 180: Exercise: Contribute to Our Open Source

---

## 181: Keeping Your Fork Up To Date

Once you are in your forked project directory in your command prompt....

1. Type `git remote -v` and press **Enter**. You'll see the current configured remote repository for your fork.
    1. git remote -v
    2. origin  [https://github.com/YOUR_USERNAME/YOUR_FORK.git](https://github.com/YOUR_USERNAME/YOUR_FORK.git) (fetch)
    3. origin  [https://github.com/YOUR_USERNAME/YOUR_FORK.git](https://github.com/YOUR_USERNAME/YOUR_FORK.git) (push)
2. Type `git remote add upstream`, and then paste the URL you would copy from the original repository if you were to do a git clone. Press **Enter**. It will look like this:
    1. git remote add upstream [https://github.com/zero-to-mastery/PROJECT_NAME.git](https://github.com/zero-to-mastery/PROJECT_NAME.git)
3. To verify the new upstream repository you've specified for your fork, type `git remote -v` again. You should see the URL for your fork as `origin`, and the URL for the original repository as `upstream`.
    1. git remote -v
    2. origin    [https://github.com/YOUR_USERNAME/YOUR_FORK.git](https://github.com/YOUR_USERNAME/YOUR_FORK.git) (fetch)
    3. origin    [https://github.com/YOUR_USERNAME/YOUR_FORK.git](https://github.com/YOUR_USERNAME/YOUR_FORK.git) (push)
    4. upstream  [https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git](https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git) (fetch)
    5. upstream  [https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git](https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git) (push)

Now, you can keep your fork synced with the upstream repository with a few Git commands.

One simple way is to do the below command from the master of your forked repository:

```bash
git pull upstream master
```

Or you can follow along another method here: "[Syncing a fork](https://help.github.com/articles/syncing-a-fork)."

---

## 182: Create a Portfolio

[Simplefolio](https://github.com/cobidev/simplefolio)

[hatchful.shopify.com](https://hatchful.shopify.com)

---

## 183: Portfolio Website for Recruiters

[Z2M Job Board](https://zero-to-mastery.github.io/ZtM-Job-Board/)

---

## 184: Student Generated Top Resources

[Z2M Resources](https://zero-to-mastery.github.io/resources/)

---
