# Databases

---

## 283: Intro to Databases

Basic database info.

Discussed relational databases (e.g. PostgreSQL) and non-relational databases (e.g. MongoDB)

---

## 284: Installing PostgreSQL

Installed version 3 of PGAdmin

---

## 285: For WindowsUsers

N/A

---

## 286: Resources: Installing PostgreSQL

Great doc on Linux Mint installation - [Link](Section26_Databases.assets/Install.docx)

---

## 287: SQL: Create Table

### Commands

```sql
-- create a new table and add columns
CREATE TABLE users(name text, age smallint, birthday date)

-- view table info
\d

-- exit
\q
```

---

## 288: SQL: Insert Into + Select

```sql
-- insert
INSERT INTO users (name, age, birthday) VALUES ('Andrei',31, '1930-01-25');

-- select
SELECT name,age, birthday FROM users;
-- or
SELECT * FROM users;
```

---

## 289: SQL Alter Table + Update

```sql
-- add a new column
ALTER TABLE users ADD score smallint;

-- update data
UPDATE users SET score = 50 WHERE name='Andrei';
UPDATE users SET score = 100 WHERE name='John' OR name='Sally';
```

---

## 290: SQL: Conditional Selections

```sql
test=# SELECT * FROM users WHERE name LIKE 'A%'; --case sensitive
  name  | age |  birthday  | score
--------+-----+------------+-------
 Andrei |  31 | 1930-01-25 |    50
 Amy    |  15 | 1935-04-04 |    88
(2 rows)

------------------------------------------------------

SELECT * FROM users ORDER BY score desc;
  name  | age |  birthday  | score
--------+-----+------------+-------
 Sally  |  41 | 1930-01-04 |   100
 John   |  45 | 1935-04-04 |   100
 Amy    |  15 | 1935-04-04 |    88
 Andrei |  31 | 1930-01-25 |    50
(4 rows)

```

---

## 291: SQL Functions

```sql
test=# SELECT AVG(score) FROM users;
         avg
---------------------
 84.5000000000000000
(1 row)


test=# SELECT SUM(age) FROM users;
 sum
-----
 132
(1 row)

test=# SELECT COUNT(name) FROM users;
 count
-------
     4
(1 row)
```

---

## 292: Joining Tables Part 1

```sql
-- made a new login table wiht a primary key
CREATE TABLE login (
ID serial NOT NULL PRIMARY KEY,
secret VARCHAR (100) NOT NULL,
name text UNIQUE NOT NULL
);

-- adding info; ID is a serial, so it is added automatically
INSERT INTO login
(secret, name)
VALUES ('abc', 'Andrei')
;

INSERT INTO login
(secret, name)
VALUES ('xyz', 'Sally')
;

INSERT INTO login
(secret, name)
VALUES ('lol', 'John')
;
```

![image](./images/image-20191111085940138.png)

Note that John has an ID of 4, because we attempted to add Sally a second time, producing an error.

---

## 293: Joining Tables Part 2

```sql
-- when we create a unique id, a new table is automatically created
test=# \d
            List of relations
 Schema |     Name     |   Type   | Owner
--------+--------------+----------+-------
 public | login        | table    | steve
 public | login_id_seq | sequence | steve
 public | users        | table    | steve
(3 rows)


-- JOIN
test=# SELECT * FROM users JOIN login ON users.name = login.name;
  name  | age |  birthday  | score | id | secret |  name  
--------+-----+------------+-------+----+--------+--------
 Andrei |  31 | 1930-01-25 |    50 |  1 | abc    | Andrei
 Sally  |  41 | 1930-01-04 |   100 |  2 | xyz    | Sally
 John   |  45 | 1935-04-04 |   100 |  4 | lol    | John
(3 rows)
```

---

## 294: SQL: Delete From + Drop Table

```sql
-- delete a user
test=# DELETE FROM users WHERE name='Sally';
DELETE 1
test=# SELECT * FROM users
test-# ;
  name  | age |  birthday  | score
--------+-----+------------+-------
 Andrei |  31 | 1930-01-25 |    50
 John   |  45 | 1935-04-04 |   100
 Amy    |  15 | 1935-04-04 |    88
(3 rows)

-- delete a table
test=# DROP TABLE login;
DROP TABLE
test=# DROP TABLE users;
DROP TABLE
test=# \d
Did not find any relations.
```

---

## 295: Exercises: SQL Commands

[Khan Academy](https://www.khanacademy.org/computing/computer-programming/sql)

[Code Academy](https://www.codecademy.com/learn/learn-sql)

---
