# Sections 09 - 10

## Section 09: CSS Grid + CSS Layout

---

### Lesson 79: Section Overview

Figuring out a responsive layout is tough. This section will cover that.
We will develop a template, and learn CSS Grid.

---

### Lesson 80: CSS Grid vs Flexbox vs Bootstrap

CSS Grids are powerful when it comes to 2-dimensional layouts (compared to Flexbox).
Bootstrap allows this too. Bootstrap is older. Flexbox and CSS Grid are newer and allow us to do everything Bootstrap does.  CSS Grid is newest, and has the least support (although it is pretty standard in newer browsers).

---

### Lesson 81: CSS Grid 1

- [index.html](../LessonFiles/081/index.html)
- [style.css](../LessonFiles/081/style.css)

We added the container class to the top of the CSS file, and gave it these characteristics:

#### style.css

```css
.container{
    display:grid;
    grid-gap: 20px;
    grid-template-columns:  1fr 1fr 2fr;
}
........
```

We added the `div` around the emojis in the html file

#### index.html

```html
<!DOCTYPE html>
<html>
<head>
  <title>Grid Master</title>
  <link rel="stylesheet" type="text/css" href="./style.css">
</head>
<body>
    <div class="container">
      <div class="zone green">🦊</div>
      <div class="zone red">🐰</div>
      <div class="zone blue">🐸</div>
      <div class="zone yellow">🦁</div>
      <div class="zone purple">🐯</div>
      <div class="zone brown">🐭</div>
      <div class="zone green">🦄</div>
      <div class="zone red">🐲</div>
      <div class="zone blue">🐷</div>
      <div class="zone yellow">🐺</div>
      <div class="zone purple">🐼</div>
      <div class="zone brown">🐻</div>
    </div>
</body>
</html>
```

![image](./images/Lesson81a.png)

---

### Lesson 82: CSS Grid 2

Covered `auto`

```css
.container{
    display:grid;
    grid-gap: 20px;
    grid-template-columns:  auto 1fr, 2fr;
    grid-template-rows: 1fr;
}
```

---

### Lesson 83: CSS Grid 3

Covered `repeat`, `auto-fill`, `minmax`

```css
.container{
    display:grid;
    grid-gap: 20px;
    grid-template-columns:  repeat(auto-fill, minmax(200px, 1fr));
    grid-template-rows: 1fr;
}
```

---

### Lesson 84: CSS Grid 4

```css
.container{
    display:grid;
    grid-gap: 20px;
    grid-template-columns:  repeat(auto-fill, minmax(200px, 1fr));
    grid-template-rows: 1fr;
}
.green{
    /* grid-column: 1/3;  makes green item span from first line to 3rd */
    /* grid-column: 1/-1;  would go all the way to end */
    grid-column: span 2;  /* spans 2 columns, can do same with rows */
    grid-row: 1/3;     /* spans from line 1 to 3 */
}
```

![image](./images/Lesson84a.png)

---

### Lesson 85: CSS Grid 5

Just as we can use `justify-items` for a section, we can use `justify-self` for items

```css
.green{
    grid-column: span 2;
    grid-row: 1/3;
    justify-self: start;
}
```

- [A great website for CSS Grid](http://grid.malven.co/)

---

### Lesson 86: (Optional Exercise) CSS Grid Garden

- [Grid Garden](http://cssgridgarden.com/)

---

### Lesson 87: Exercise: CSS Layout

#### index.html (initial)

```html
<!DOCTYPE html>
<html>
<head>
  <title>Layout Master</title>
  <link rel="stylesheet" type="text/css" href="./style.css">
</head>
<body>
  <div class="container">
    <div class="zone green">Header</div>
    <div class="zone red">Cover</div>
    <div class="zone blue">Project Grid</div>
    <div class="zone yellow">Footer</div>
</div>
</body>
</html>
```

#### style.css

```css
.container{
    display: grid;
    grid-template-rows: 1fr;
}

.zone {
    padding:30px 50px;
    margin:40px 60px;
    cursor:pointer;
    display:inline-block;
    color:#FFF;
    font-size:2em;
    border-radius:4px;
    border:1px solid #bbb;
    transition: all 0.3s linear;
}
.zone:hover {
    -webkit-box-shadow:rgba(0,0,0,0.8) 0px 5px 15px, inset rgba(0,0,0,0.15) 0px -10px 20px;
    -moz-box-shadow:rgba(0,0,0,0.8) 0px 5px 15px, inset rgba(0,0,0,0.15) 0px -10px 20px;
    -o-box-shadow:rgba(0,0,0,0.8) 0px 5px 15px, inset rgba(0,0,0,0.15) 0px -10px 20px;
    box-shadow:rgba(0,0,0,0.8) 0px 5px 15px, inset rgba(0,0,0,0.15) 0px -10px 20px;
}
/*https://paulund.co.uk/how-to-create-shiny-css-buttons*/
/***********************************************************************
 *  Green Background
 **********************************************************************/
.green{
    grid-row: 1/2;
    background: #56B870; /* Old browsers */
    background: -moz-linear-gradient(top, #56B870 0%, #a5c956 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#56B870), color-stop(100%,#a5c956)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #56B870 0%,#a5c956 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #56B870 0%,#a5c956 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #56B870 0%,#a5c956 100%); /* IE10+ */
    background: linear-gradient(top, #56B870 0%,#a5c956 100%); /* W3C */
}
/***********************************************************************
 *  Red Background
 **********************************************************************/
.red{
    grid-row: 2/4;
    background: #C655BE; /* Old browsers */
    background: -moz-linear-gradient(top, #C655BE 0%, #cf0404 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#C655BE), color-stop(100%,#cf0404)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #C655BE 0%,#cf0404 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #C655BE 0%,#cf0404 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #C655BE 0%,#cf0404 100%); /* IE10+ */
    background: linear-gradient(top, #C655BE 0%,#cf0404 100%); /* W3C */
}
/***********************************************************************
 *  Yellow Background
 **********************************************************************/
.yellow{
    grid-row 6/7;
    background: #F3AAAA; /* Old browsers */
    background: -moz-linear-gradient(top, #F3AAAA 0%, #febf04 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#F3AAAA), color-stop(100%,#febf04)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #F3AAAA 0%,#febf04 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #F3AAAA 0%,#febf04 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #F3AAAA 0%,#febf04 100%); /* IE10+ */
    background: linear-gradient(top, #F3AAAA 0%,#febf04 100%); /* W3C */
}
/***********************************************************************
 *  Blue Background
 **********************************************************************/
.blue{
    grid-row: 4/6;
    background: #7abcff; /* Old browsers */
    background: -moz-linear-gradient(top, #7abcff 0%, #60abf8 44%, #4096ee 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#7abcff), color-stop(44%,#60abf8), color-stop(100%,#4096ee)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* IE10+ */
    background: linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* W3C */
}
```

---

### Lesson 88: Solution: Navigation Bar

#### index.html

```html
<!DOCTYPE html>
<html>
<head>
  <title>Layout Master</title>
  <link rel="stylesheet" type="text/css" href="./style.css">
</head>
<body>
  <nav class="zone green">
    <ul class="main-nav">
      <li><a href="">About</li>
      <li><a href="">Products</li>
      <li><a href="">Our Team</li>
      <li class="push"><a href="">Contacts</li>

    </ul>
  </div>
  <div class="zone red">Cover</div>
  <div class="zone blue">Project Grid</div>
  <div class="zone yellow">Footer</div>
</body>
</html>
```

#### style.css

```css
body {
    margin: auto 0;
}

.zone {
    /* padding:30px 50px;
    margin:40px 60px; */
    cursor:pointer;
    /* display:inline-block; */
    color:#FFF;
    font-size:2em;
    border-radius:4px;
    border:1px solid #bbb;
    transition: all 0.3s linear;
}
.zone:hover {
    -webkit-box-shadow:rgba(0,0,0,0.8) 0px 5px 15px, inset rgba(0,0,0,0.15) 0px -10px 20px;
    -moz-box-shadow:rgba(0,0,0,0.8) 0px 5px 15px, inset rgba(0,0,0,0.15) 0px -10px 20px;
    -o-box-shadow:rgba(0,0,0,0.8) 0px 5px 15px, inset rgba(0,0,0,0.15) 0px -10px 20px;
    box-shadow:rgba(0,0,0,0.8) 0px 5px 15px, inset rgba(0,0,0,0.15) 0px -10px 20px;
}
/* NAV */
.main-nav {
    display: flex; /* this activates flex box */
    list-style: none;
    font-size: 0.7em;
    margin: 0;
}
.push {
    margin-left: auto;
}
li {
    padding: 20px;
}
a {
    color: #f5f5f6;
    text-decoration: none;
}
```

![image](./images/s_87770798CDCDD628E415AF3CD41C6B5F9D4A7FAC4EC9E1414015667DCB5D2ABE_1551147553592_Screenshot_2019-02-25_21-18-01.png)

---

### Lesson 89: Solution: Navigation Bar II

- [CSS Tricks - Media Queries for Stnadard Devices](https://css-tricks.com/snippets/css/media-queries-for-standard-devices/)
  - This site is helpful for formatting your site for different devices (tablets, iPhones, etc) using the syntax `@media only screen`.
- CSS Grid and Flexbox resolve most of these issues.

```css
@media only screen and (max-width: 600px) {
    .main-nav: {
        font-size: 0.5em;
        padding: 0;
    }
}
```

---

### Lesson 90: Solutions Cover

#### index.html

```html
 </div>
  <div class="container zone red">Cover!!!</div>
  <div class="zone blue">Project Grid</div>
  <div class="zone yellow">Footer</div>
</body>
```

#### style.css

```css
/* COVER */
.container {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 50vh;
}
```

---

### Lesson 91: Solution: Project Grid + Footer

#### index.html

```html
 <div class="zone blue grid-wrapper">
    <div class="box zone"><img src="./img/files_2.png"></div>
    <div class="box zone"><img src="./img/server_2_2.png"></div>
    <div class="box zone"><img src="./img/monitor_settings_2.png"></div>
    <div class="box zone"><img src="./img/server_3.png"></div>
    <div class="box zone"><img src="./img/data_storage_2_2.png"></div>
    <div class="box zone"><img src="./img/monitor_coding_2.png"></div>
    <div class="box zone"><img src="./img/desktop_analytics_2.png"></div>
    <div class="box zone"><img src="./img/server_safe_2.png"></div>
  </div>
  <footer class="zone yellow">made by z => m</footer>
</div>
```

#### style.css

```css
/***********************************************************************
 *  Grid Panel
 **********************************************************************/
.grid-wrapper {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(350px, 1fr));
    grid-gap: 20px;
}
.box {
    background-color: #444;
    padding: 130px;
    margin: 20px;
}
.box > img {
    width: 100%;
}
```

---

### Lesson 92: Solution: Prettify

[interfacer.xyz](https://interfacer.xyz/)

#### index.html

```html
<!DOCTYPE html>
<html>
<head>
  <title>Layout Master</title>
  <link rel="stylesheet" type="text/css" href="./style.css">
</head>
<body>
  <nav class="zone blue sticky">
      <ul class="main-nav">
          <li><a href="">About</a></li>
          <li><a href="">Products</a></li>
          <li><a href="">Our Team</a></li>
          <li class="push"><a href="">Contact</a></li>
  </ul>
  </nav>
  <div class="container"><img class="cover" src="./img/undraw.png"></div>
  <div class="zone blue grid-wrapper">
    <div class="box zone"><img src="./img/files_2.png"></div>
    <div class="box zone"><img src="./img/server_2_2.png"></div>
    <div class="box zone"><img src="./img/monitor_settings_2.png"></div>
    <div class="box zone"><img src="./img/server_3.png"></div>
    <div class="box zone"><img src="./img/data_storage_2_2.png"></div>
    <div class="box zone"><img src="./img/monitor_coding_2.png"></div>
    <div class="box zone"><img src="./img/desktop_analytics_2.png"></div>
    <div class="box zone"><img src="./img/server_safe_2.png"></div>
  </div>
  <footer class="zone yellow">made by z => m</footer>
</div>
</body>
</html>
```

#### style.css

```css
body {
    margin: 0 auto;
    font-family: Arial;
}
.zone {
    /*padding:30px 50px;*/
    cursor:pointer;
    color:#FFF;
    font-size:2em;
    border-radius:4px;
    border:1px solid #bbb;
    transition: all 0.3s linear;
}
.zone:hover {
    -webkit-box-shadow:rgba(0,0,0,0.8) 0px 5px 15px, inset rgba(0,0,0,0.15) 0px -10px 20px;
    -moz-box-shadow:rgba(0,0,0,0.8) 0px 5px 15px, inset rgba(0,0,0,0.15) 0px -10px 20px;
    -o-box-shadow:rgba(0,0,0,0.8) 0px 5px 15px, inset rgba(0,0,0,0.15) 0px -10px 20px;
    box-shadow:rgba(0,0,0,0.8) 0px 5px 15px, inset rgba(0,0,0,0.15) 0px -10px 20px;
}
/***********************************************************************
 *  Nav Bar
 **********************************************************************/
.main-nav {
    display: flex;
    list-style: none;
    margin: 0;
    font-size: 0.7em;
}
@media only screen and (max-width: 600px) {
    .main-nav {
        font-size: 0.5em;
        padding: 0;
    }
}
.push {
    margin-left: auto;
}
li {
    padding: 20px;
}
a {
    color: #f5f5f6;
    text-decoration: none;
}
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}
/***********************************************************************
 *  Cover
 **********************************************************************/
.container {
    /*show heigh: auto*/
    height: 70vh;
    display: flex;
    align-items: center;
    justify-content: center;
}
.cover {
    width: 30rem;
}
/***********************************************************************
 *  Grid Panel
 **********************************************************************/
.grid-wrapper {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(350px, 1fr));
    grid-gap: 20px;
}
.box {
    background-color: #444;
    padding: 130px;
    margin: 20px;
}
.box > img {
    width: 100%;
}
.box:hover {
    -webkit-transform: rotate(-7deg);
    -moz-transform: rotate(-7deg);
    -o-transform: rotate(-7deg);
    transform: rotate(-7deg);
}
.box > img {
    width: 100%;
    height: auto;
}
/***********************************************************************
 *  Footer
 **********************************************************************/
footer {
    text-align: center;
    background-color: #444;
}
/*https://paulund.co.uk/how-to-create-shiny-css-buttons*/
/***********************************************************************
 *  Green Background
 **********************************************************************/
.green {
    background: #56B870; /* Old browsers */
    background: -moz-linear-gradient(top, #56B870 0%, #a5c956 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#56B870), color-stop(100%,#a5c956)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #56B870 0%,#a5c956 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #56B870 0%,#a5c956 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #56B870 0%,#a5c956 100%); /* IE10+ */
    background: linear-gradient(top, #56B870 0%,#a5c956 100%); /* W3C */
}
/***********************************************************************
 *  Red Background
 **********************************************************************/
.red {
    background: #C655BE; /* Old browsers */
    background: -moz-linear-gradient(top, #C655BE 0%, #cf0404 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#C655BE), color-stop(100%,#cf0404)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #C655BE 0%,#cf0404 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #C655BE 0%,#cf0404 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #C655BE 0%,#cf0404 100%); /* IE10+ */
    background: linear-gradient(top, #C655BE 0%,#cf0404 100%); /* W3C */
}
/***********************************************************************
 *  Yellow Background
 **********************************************************************/
.yellow {
    background: #F3AAAA; /* Old browsers */
    background: -moz-linear-gradient(top, #F3AAAA 0%, #febf04 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#F3AAAA), color-stop(100%,#febf04)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #F3AAAA 0%,#febf04 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #F3AAAA 0%,#febf04 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #F3AAAA 0%,#febf04 100%); /* IE10+ */
    background: linear-gradient(top, #F3AAAA 0%,#febf04 100%); /* W3C */
}
/***********************************************************************
 *  Blue Background
 **********************************************************************/
.blue {
    background: #7abcff; /* Old browsers */
    background: -moz-linear-gradient(top, #7abcff 0%, #60abf8 44%, #4096ee 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#7abcff), color-stop(44%,#60abf8), color-stop(100%,#4096ee)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* IE10+ */
    background: linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* W3C */
}
```

---

### Lesson 93: The Truth About CSS

Basically just advice - you don’t need to be an expert on HTML before learning CSS, or an expert on both before learning JS.

---

---

## Section 10: Career of a Web Developer

### Lesson 94: Career of a Web Developer

Nice video on types of web developers and why he chose what to include in the course.

---

### Lesson 95: Developer Fundamentals VI

Primarily focused on Stack Overflow and using the resources you have.

---

### Lesson 95 & 96:  Discord and LinkedIn Notes

Just note on moving discussion to Discord and a LinkedIn group for ZTM.

---

### Lesson 97 & 98: What If I Don’t Have Enough Experience?

Good general guidelines. The main points - ways to show you have experience, when you don’t have official experience:

- Github
- Website
- 1-2 big projects
- Blog

---

### Lesson 99: Learning Guideline

![image](./images/99.jpg)

---
