# Section 12: DOM Manipulation

---

## 122: Document Object Model

Back to building webpages. But now, with Javascript, we can:

- change all the HTML elements in the page
- change all the HTML attributes in the page
- change all the CSS styles in the page
- remove existing HTML elements and attributes
- add new HTML elements and attributes
- Javascript can react to all existing HTML events in the page
- Javascript can create new HTML events in the page

### The Document Object Model (DOM)

![The HTML DOM Tree of Objects](./images/s_7D1202C3D5F8FBE7EAE916DF11828EE5A3F4E66399355666EB19DFBEBF035AE6_1554247825973_122DOM.gif)

```js
document.write("Hellooooooo");
```

- The “Document” in DOM refers to an actual object. We can refer to it, as in this example above, which causes the webpage to simply say “Helloooooo”.
- Document is the what you see in the browser.
- Document has a parent object: Window
- Example: `window.alert(``"``check, check``"``);` will cause a pop-up that says, “check check”
  - Any time you specify a method, the browser assumes you are referring to the ‘window’ object.

---

## 123: DOM Selectors

### `getElementsByClassName`, `getElementByID`

```js
document.getElementsByClassName("second")
    1. HTMLCollection [p.second]
      1. 0: p.second
      2. length: 1
      3. __proto__: HTMLCollection

document.getElementById("first");
    1. <p id="first">Get it done today</p>

document.getElementsByClassName("second")[0]
    1. <p class="second">No excuses</p>
```

### `querySelector`, `querySelectorAll`

```js
document.querySelector("H1");
    1. <h1>Shopping List</h1>

document.querySelector("li");  // only selects first
    1. <li random="23">Notebook</li>

document.querySelectorAll("li");  // this covers all
    1. NodeList(6) [li, li, li, li, li, li]
      1. 0: li
      2. 1: li
      3. 2: li
      4. 3: li
      5. 4: li
      6. 5: li
      7. length: 6
      8. __proto__: NodeList

document.querySelectorAll("h1, li");
    1. NodeList(7) [h1, li, li, li, li, li, li]
```

### `getAttribute`, `setAttribute`

```js
document.querySelector("li");
<li random="23">Notebook</li>

document.querySelector("li").getAttribute("random");
"23"

document.querySelector("li").setAttribute("random", "1000");
undefined

document.querySelector("li").getAttribute("random");
"1000"
```

### `querySelector`, `style.(property)`

```js
document.querySelector("H1");
    1. <h1>Shopping List</h1>

document.querySelector("H1").style
    1. CSSStyleDeclaration {alignContent: "", alignItems: "", alignSelf: "", alignmentBaseline: "", all: "", …}

document.querySelector("H1").style.background = "yellow"
"yellow"

// we can do this, but it violates the separation of html/css/js
```

```js
var h1 = document.querySelector("h1");
undefined

h1
    1. <h1 style="background: yellow;">Shopping List</h1>

h1.className = "coolTitle";
"coolTitle"

h1
    1. <h1 style="background: yellow;" class="coolTitle">Shopping List</h1>
```

```js
document.querySelector("li").classList;
    1. DOMTokenList [value: ""]

// after adding classes in html

document.querySelector("li").classList;
    1. DOMTokenList(2) ["bold", "red", value: "bold red"]
```

### `className`, `classList.add`, `.remove`, `.toggle`

```js
document.querySelector("li").classList;
    1. DOMTokenList(2) ["bold", "red", value: "bold red"]

document.querySelector("li").classList.add("coolTitle");
undefined

document.querySelector("li").classList;
    1. DOMTokenList(3) ["bold", "red", "coolTitle", value: "bold red coolTitle"]

document.querySelector("li").classList.remove("coolTitle");
undefined

document.querySelector("li").classList.add("done");
undefined

document.querySelector("li").classList;
    1. DOMTokenList(3) ["bold", "red", "done", value: "bold red done"]

document.querySelector("li").classList.toggle("done");
false

document.querySelector("li").classList.toggle("done");
true
```

### innerHTML

```js
document.querySelector("h1").innerHTML = "<strong>!!!!!!</strong>"
"<strong>!!!!!!</strong>"

// this just changed the inner HTML of h1

document.querySelector("h1");
    1. <h1>
      1. <strong>!!!!!!</strong>
      2. </h1>
```

### `.parent` and `.child` elements

```js
document.querySelectorAll("li")[1];
    1. <li>Jello</li>
document.querySelectorAll("li")[1].parentElement;
    1. <ul>…</ul>

// .children also works
```

### CACHE selectors in variables

```js
// by storing as a variable, the browser doesn't need to look up the item each time
```

---

## 124: Exercise: DOM Selectors

```text
DOM Selectors
-------
getElementsByTagName
getElementsByClassName
getElementById

querySelector
querySelectorAll

getAttribute
setAttribute

##Changing Styles
style.{property} //ok

className //best
classList //best

classList.add
classList.remove
classList.toggle

##Bonus
innerHTML //DANGEROUS

parentElement
children

##It is important to CACHE selectors in variables
```

---

## 125: DOM Events

```js
var button = document.getElementsByTagName("button")[0];
    // [0] for first element with Tag Name "button"
    // below says, addEventListener for "click", and execute this function
button.addEventListener("click", function() {
    console.log("CLICK!!!");
})
button.addEventListener("mouseenter", function() {
    console.log("MouseEnter!!!");
})
```

![Output of above code](./images/125a.png)

### [DOM Event List](https://developer.mozilla.org/en-US/docs/Web/Events)

### [JavaScript Key Codes](https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes)

Now to add a button and textbox for adding new items to the list.

First step, capture the event click.  Use `console.log(``"``working``"``);` to check and see if something is working before proceeding

```js
var button = document.getElementById("enter");
var input = document.getElementById("userinput");

button.addEventListener("click", function() {
    console.log("click is working"); // a good way to test
})
```

Next step: adding an item to the list

```js
var button = document.getElementById("enter");
var input = document.getElementById("userinput");
var ul = document.querySelector("ul");

button.addEventListener("click", function() {
    var li = document.createElement("li"); // on click, create a new 'li' element
    li.appendChild(document.createTextNode("testing")); // add 'li' child with "testing"; change this to input.value (no quotes) to get the text box value
    ul.appendChild(li); // add it to the end of the 'ul'
})
```

### Cleaning it up:

```js
var button = document.getElementById("enter");
var input = document.getElementById("userinput");
var ul = document.querySelector("ul");

// function to return input length
function inputLength() {
    return input.value.length;
}

// function to create a list element, then reset the input to a blank string
function createListElement() {
    var li = document.createElement("li");
    li.appendChild(document.createTextNode(input.value));
    ul.appendChild(li);
    input.value = "";
}

// adds item after click
function addListAfterClick() {
    if (inputLength() > 0) {
        createListElement();
    }
}

// adds item after Enter
function addListAfterKeypress(event) {
    if (inputLength() > 0 && event.keyCode === 13) {
        createListElement();
    }
}

// The event listeners,to act when the event occurs
button.addEventListener("click", addListAfterClick)
input.addEventListener("keypress", addListAfterKeypress)
```

---

## 126: Note: Callback Functions

In the previous video you saw something interesting:
Event listener syntax :

```js
button.addEventListener("click", addListAfterClick);
input.addEventListener("keypress", addListAfterKeypress);
```

You didn't see the function being called like this as you may have expected:

```js
button.addEventListener("click", addListAfterClick());
input.addEventListener("keypress", addListAfterKeypress(event));
```

This is something called a callback function. When that line of Javascript runs, we don't want the addListAfterClick function to run because we are just adding the event listener now to wait for click or keypress. We want to let it know though that we want this action to happen when a click happens. So the function now automatically gets run (gets added the ()) every time the click happens. So we are passing a reference to the function without running it.

---

## 127/128: Exercise: DOM Events

This was tough. Used some examples, but think I understand it

### index.html

```html
<!DOCTYPE html>
<html>
<head>
    <title>Javascript + DOM</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <h1>Shopping List</h1>
    <p id="first">Get it done today</p>
    <input id="userinput" type="text" placeholder="enter items">
    <button id="enter">Enter</button>
    <ul>
    </ul>
    <script type="text/javascript" src="script.js"></script>
</body>
</html>
```

### style.css

```css
.coolTitle {
  text-align: center;
  font-family: 'Oswald', Helvetica, sans-serif;
  font-size: 40px;
  transform: skewY(-10deg);
  letter-spacing: 4px;
  word-spacing: -8px;
  color: tomato;
  text-shadow:
    -1px -1px 0 firebrick,
    -2px -2px 0 firebrick,
    -3px -3px 0 firebrick,
    -4px -4px 0 firebrick,
    -5px -5px 0 firebrick,
    -6px -6px 0 firebrick,
    -7px -7px 0 firebrick,
    -8px -8px 0 firebrick,
    -30px 20px 40px dimgrey;
}
.done {
  text-decoration: line-through;
}
/* New code below */
.deleteButton { /* this creates the delete button */
  background-color: #a0829b;
  color: #fff;
  border-radius: 30px
  margin: 20px;
}
```

### script.js

```js
var button = document.getElementById("enter");
var input = document.getElementById("userinput");
var ul = document.querySelector("ul");
function inputLength() {
    return input.value.length;
}

function createListElement() {
    var li = document.createElement("li");
    li.appendChild(document.createTextNode(input.value));
    ul.appendChild(li);

    // within the function that creates the list element
    // this will listen for a click on "this" element only
    li.addEventListener("click", function() {
        var finished = this.classList.toggle("done");
        // create a remove button
        var removeButton = document.createElement("button");
        // removeButton.classList.add("deleteButton");
        // if finished (clicked)
        if (finished) {
            // makes removeButton with 'remove' text
            removeButton.appendChild(document.createTextNode("remove"));
            // gives it the class "deleteButton", then adds it to list
            removeButton.classList = "deleteButton";
            li.appendChild(removeButton);
            // now if remove button is clicked
            removeButton.addEventListener("click", function() {
                this.parentElement.remove();
            });
        } else {
            this.getElementsByClassName("deleteButton")[0].remove();
        }
    })
    input.value = "";
}
function addListAfterClick() {
    if (inputLength() > 0) {
        createListElement();
    }
}
function addListAfterKeypress(event) {
    if (inputLength() > 0 && event.keyCode === 13) {
        createListElement();
    }
}
button.addEventListener("click", addListAfterClick);
input.addEventListener("keypress", addListAfterKeypress);
```

---

## 129: Exercise: Background Generator

### index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Gradient Background</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body id="gradient">
    <h1>Background Generator</h1>
    <h2>Current CSS Background</h2>
    <input class="color1" type="color" name="color1" value="#00ff00">
    <!-- could add oninput="setGradient()"
    instead of addEventListener in js file (best)-->
    <input class="color2" type="color" name="color2" value="#ff0000">
    <h3></h3>
    <script type="text/javascript" src="script.js"></script>
</body>
</html>
```

### style.css

```css
body {
    font: 'Raleway', sans-serif;
    color: rgba(0,0,0,.5);
    text-align: center;
    text-transform: uppercase;
    letter-spacing: .5em;
    top: 15%;
    background: linear-gradient(to right, red , yellow); /* Standard syntax */
}
h1 {
    font: 600 3.5em 'Raleway', sans-serif;
    color: rgba(0,0,0,.5);
    text-align: center;
    text-transform: uppercase;
    letter-spacing: .5em;
    width: 100%;
}
h3 {
    font: 900 1em 'Raleway', sans-serif;
    color: rgba(0,0,0,.5);
    text-align: center;
    text-transform: none;
    letter-spacing: 0.01em;
}
```

### script.js

```js
 var css = document.querySelector("h3");
 var color1 = document.querySelector(".color1");
 var color2 = document.querySelector(".color2");
 var body = document.getElementById("gradient");
// body.style.background = "red";
//  console.log(css);
//  console.log(color1);
//  console.log(color2);
function setGradient() {
    body.style.background =
    "linear-gradient(to right, "
    + color1.value
    + ", "
    + color2.value
    + ")";
    css.textContent = body.style.background + ";"
}
color1.addEventListener("input", setGradient);
color2.addEventListener("input", setGradient);
```

---

## 130: Background Generator Files

Just files for above.

---

## 131: jQuery

[You Might Not Need jQuery](http://youmightnotneedjquery.com)

- jQuery is older, and not used as often. In this course, we will learn React instead.

---

## 132: Developer Fundamentals V

Minimize DOM manipulation. Excessive changes cause website to reload often, slowing it down.

---
