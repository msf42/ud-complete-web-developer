# FINAL PROJECT: SmartBrain Back-End Database

---

## 296: Setting Up Your Database

```sql
-- creating our tables
CREATE TABLE users (
id serial PRIMARY KEY,
name varchar(100),
email text UNIQUE NOT NULL,
entries bigint DEFAULT 0,
joined TIMESTAMP NOT NULL
);

CREATE TABLE login (
id serial PRIMARY KEY,
hash varchar(100) NOT NULL,
email text UNIQUE NOT NULL
);
```

---

## 297: Connecting to the Database

### server.js

```js
const express = require('express');
const bcrypt = require('bcrypt-nodejs');
const app = express();
const cors = require('cors');
const knex = require ('knex');

app.use(express.json());
app.use(cors());

const postgres = knex({
  client: 'pg',
  connection: {
    host : '127.0.0.1',
    user : 'steve',
    password : '',
    database : 'smart-brain'
  }
});

console.log(postgres.select('*').from('users'));

const database ={
  users: [
    {
      id: '123',
      name: 'John',
      password: 'cookies',
      email: 'john@gmail.com',
      entries: 0,
      joined: new Date()
    },
    {
      id: '124',
      name: 'Sally',
      password: 'bananas',
      email: 'sally@gmail.com',
      entries: 0,
      joined: new Date()
    }
  ],
  login: [
    {
      id: '987',
      hash: '',
      email: 'john@gmail.com'
    }
  ]
}


app.get('/', (req, res)=> {
  res.send(database.users)
})

app.post('/signin', (req, res) => {
  // Load hash from your password DB.
  bcrypt.compare("apples", "$2a$10$sbAtpDOfPXFH7n0JmktsKeqX9AfnHspsiyVKWVv6X1SdfvLVr6yOa", function(err, res) {
  console.log("first guess", res)
  });
  bcrypt.compare("veggies", "$2a$10$sbAtpDOfPXFH7n0JmktsKeqX9AfnHspsiyVKWVv6X1SdfvLVr6yOa", function(err, res) {
  console.log('second guess', res)
  });
  if (req.body.email === database.users[0].email &&
      req.body.password === database.users[0].password) {
      res.json(database.users[0]);
    } else {
      res.status(400).json('error logging in');
    }
})

app.post('/register', (req, res) => {
  const { email, name, password } = req.body;
  database.users.push({
    id: '125',
    name: name,
    email: email,
    entries: 0,
    joined: new Date()
  })
  res.json(database.users[database.users.length - 1]);
})

app.get('/profile/:id', (req, res) => {
  const { id } = req.params;
  let found = false;
  database.users.forEach(user => {
    if (user.id === id) {
      found = true;
      return res.json(user);
    }
  })
  if (!found) {
    res.status(400).json('not found')
  }
})

app.put('/image', (req, res) => {
  const { id } = req.body;
  let found = false;
  database.users.forEach(user => {
    if (user.id === id) {
      found = true;
      user.entries++;
      return res.json(user.entries);
    }
  })
  if (!found) {
    res.status(400).json('not found')
  }
})

app.listen(4000, ()=> {
  console.log('app is running on port 3000');
});
```

---

## 298: Registering a User: Part 1

### server.js

```js
const express = require('express');
const bcrypt = require('bcrypt-nodejs');
const app = express();
const cors = require('cors');
const knex = require ('knex');

app.use(express.json());
app.use(cors());

const db = knex({
  client: 'pg',
  connection: {
    host : '127.0.0.1',
    user : 'steve',
    password : 'Marglar',
    database : 'smart-brain'
  }
});


const database ={
  users: [
    {
      id: '123',
      name: 'John',
      password: 'cookies',
      email: 'john@gmail.com',
      entries: 0,
      joined: new Date()
    },
    {
      id: '124',
      name: 'Sally',
      password: 'bananas',
      email: 'sally@gmail.com',
      entries: 0,
      joined: new Date()
    }
  ],
  login: [
    {
      id: '987',
      hash: '',
      email: 'john@gmail.com'
    }
  ]
}


app.get('/', (req, res)=> {
  res.send(database.users)
})

app.post('/signin', (req, res) => {
  // Load hash from your password DB.
  bcrypt.compare("apples", "$2a$10$sbAtpDOfPXFH7n0JmktsKeqX9AfnHspsiyVKWVv6X1SdfvLVr6yOa", function(err, res) {
  console.log("first guess", res)
  });
  bcrypt.compare("veggies", "$2a$10$sbAtpDOfPXFH7n0JmktsKeqX9AfnHspsiyVKWVv6X1SdfvLVr6yOa", function(err, res) {
  console.log('second guess', res)
  });
  if (req.body.email === database.users[0].email &&
      req.body.password === database.users[0].password) {
      res.json(database.users[0]);
    } else {
      res.status(400).json('error logging in');
    }
})

app.post('/register', (req, res) => {
  const { email, name, password } = req.body;
  db('users').insert({
    email: email,
    name: name,
    joined: new Date()
  }).then(console.log)
  res.json(database.users[database.users.length - 1]);
})

app.get('/profile/:id', (req, res) => {
  const { id } = req.params;
  let found = false;
  database.users.forEach(user => {
    if (user.id === id) {
      found = true;
      return res.json(user);
    }
  })
  if (!found) {
    res.status(400).json('not found')
  }
})

app.put('/image', (req, res) => {
  const { id } = req.body;
  let found = false;
  database.users.forEach(user => {
    if (user.id === id) {
      found = true;
      user.entries++;
      return res.json(user.entries);
    }
  })
  if (!found) {
    res.status(400).json('not found')
  }
})

app.listen(4000, ()=> {
  console.log('app is running on port 4000');
});
```

```sql
smart-brain=# SELECT * FROM users;
 id | name |     email     | entries |         joined
----+------+---------------+---------+-------------------------
  1 | Ann  | ann@gmail.com |       0 | 2019-11-11 10:48:56.867
(1 row)

```

---

## 299: Troubleshooting: Connecting Knex

No issues

---

## 300: Registering a User: Part 2

### server.js

```js
const express = require('express');
const bcrypt = require('bcrypt-nodejs');
const app = express();
const cors = require('cors');
const knex = require ('knex');

app.use(express.json());
app.use(cors());

const db = knex({
  client: 'pg',
  connection: {
    host : '127.0.0.1',
    user : 'steve',
    password : 'Marglar',
    database : 'smart-brain'
  }
});


const database ={
  users: [
    {
      id: '123',
      name: 'John',
      password: 'cookies',
      email: 'john@gmail.com',
      entries: 0,
      joined: new Date()
    },
    {
      id: '124',
      name: 'Sally',
      password: 'bananas',
      email: 'sally@gmail.com',
      entries: 0,
      joined: new Date()
    }
  ],
  login: [
    {
      id: '987',
      hash: '',
      email: 'john@gmail.com'
    }
  ]
}


app.get('/', (req, res)=> {
  res.send(database.users)
})

app.post('/signin', (req, res) => {
  // Load hash from your password DB.
  bcrypt.compare("apples", "$2a$10$sbAtpDOfPXFH7n0JmktsKeqX9AfnHspsiyVKWVv6X1SdfvLVr6yOa", function(err, res) {
  console.log("first guess", res)
  });
  bcrypt.compare("veggies", "$2a$10$sbAtpDOfPXFH7n0JmktsKeqX9AfnHspsiyVKWVv6X1SdfvLVr6yOa", function(err, res) {
  console.log('second guess', res)
  });
  if (req.body.email === database.users[0].email &&
      req.body.password === database.users[0].password) {
      res.json(database.users[0]);
    } else {
      res.status(400).json('error logging in');
    }
})

app.post('/register', (req, res) => {
  const { email, name, password } = req.body;
  db('users')
    .returning('*')
    .insert({
      email: email,
      name: name,
      joined: new Date()
    })
    .then(user => {
      res.json(user[0]);
  })
  .catch(err => res.status(400).json('unable to register'))
})

app.get('/profile/:id', (req, res) => {
  const { id } = req.params;
  let found = false;
  database.users.forEach(user => {
    if (user.id === id) {
      found = true;
      return res.json(user);
    }
  })
  if (!found) {
    res.status(400).json('not found')
  }
})

app.put('/image', (req, res) => {
  const { id } = req.body;
  let found = false;
  database.users.forEach(user => {
    if (user.id === id) {
      found = true;
      user.entries++;
      return res.json(user.entries);
    }
  })
  if (!found) {
    res.status(400).json('not found')
  }
})

app.listen(4000, ()=> {
  console.log('app is running on port 4000');
});
```

---

## 301: Getting User Profiles

### server.js

```js
const express = require('express');
const bcrypt = require('bcrypt-nodejs');
const app = express();
const cors = require('cors');
const knex = require ('knex');

app.use(express.json());
app.use(cors());

const db = knex({
  client: 'pg',
  connection: {
    host : '127.0.0.1',
    user : 'steve',
    password : 'Marglar',
    database : 'smart-brain'
  }
});


const database ={
  users: [
    {
      id: '123',
      name: 'John',
      password: 'cookies',
      email: 'john@gmail.com',
      entries: 0,
      joined: new Date()
    },
    {
      id: '124',
      name: 'Sally',
      password: 'bananas',
      email: 'sally@gmail.com',
      entries: 0,
      joined: new Date()
    }
  ],
  login: [
    {
      id: '987',
      hash: '',
      email: 'john@gmail.com'
    }
  ]
}


app.get('/', (req, res)=> {
  res.send(database.users)
})

app.post('/signin', (req, res) => {
  // Load hash from your password DB.
  bcrypt.compare("apples", "$2a$10$sbAtpDOfPXFH7n0JmktsKeqX9AfnHspsiyVKWVv6X1SdfvLVr6yOa", function(err, res) {
  console.log("first guess", res)
  });
  bcrypt.compare("veggies", "$2a$10$sbAtpDOfPXFH7n0JmktsKeqX9AfnHspsiyVKWVv6X1SdfvLVr6yOa", function(err, res) {
  console.log('second guess', res)
  });
  if (req.body.email === database.users[0].email &&
      req.body.password === database.users[0].password) {
      res.json(database.users[0]);
    } else {
      res.status(400).json('error logging in');
    }
})

app.post('/register', (req, res) => {
  const { email, name, password } = req.body;
  db('users')
    .returning('*')
    .insert({
      email: email,
      name: name,
      joined: new Date()
    })
    .then(user => {
      res.json(user[0]);
  })
  .catch(err => res.status(400).json('unable to register'))
})

app.get('/profile/:id', (req, res) => {
  const { id } = req.params;
  db.select('*').from('users').where({id})
    .then(user => {
      if (user.length) {
        res.json(user[0])
      } else {
        res.status(400).json('Not found')
      }
  })
  .catch(err => res.status(400).json('error getting user'))
})

app.put('/image', (req, res) => {
  const { id } = req.body;
  let found = false;
  database.users.forEach(user => {
    if (user.id === id) {
      found = true;
      user.entries++;
      return res.json(user.entries);
    }
  })
  if (!found) {
    res.status(400).json('not found')
  }
})

app.listen(4000, ()=> {
  console.log('app is running on port 4000');
});
```

---

## 302: Updating Entries

### Update shown below

---

## 303: Sign In

```jsx
const express = require('express');
const bcrypt = require('bcrypt-nodejs');
const app = express();
const cors = require('cors');
const knex = require ('knex');

app.use(express.json());
app.use(cors());

const db = knex({
  client: 'pg',
  connection: {
    host : '127.0.0.1',
    user : 'steve',
    password : 'Marglar',
    database : 'smart-brain'
  }
});

app.get('/', (req, res)=> {
  res.send(database.users)
})

app.post('/signin', (req, res) => {
  db.select('email', 'hash').from('login')
  .where('email', '=', req.body.email)
    .then(data => {
      const isValid = bcrypt.compareSync(req.body.password, data[0].hash);
      if (isValid) {
        return db.select('*').from('users')
          .where('email', '=', req.body.email)
          .then(user => {
            res.json(user[0])
          })
          .catch(err => res.status(400).json('unable to get user'))
      } else {
        res.status(400).json('wrong credentials')
      }
    })
    .catch(err => res.status(400).json('wrong credentials'))
})

app.post('/register', (req, res) => {
  const { email, name, password } = req.body;
  const hash = bcrypt.hashSync(password);
  db.transaction(trx => {
    trx.insert({
      hash: hash,
      email: email
    })
    .into('login')
    .returning('email')
    .then(loginEmail => {
      return trx('users')
        .returning('*')
        .insert({
          email: loginEmail[0],
          name: name,
          joined: new Date()
        })
        .then(user => {
          res.json(user[0]);
      })
    })
    .then(trx.commit)
    .catch(trx.rollback)
  })
    return db('users')

  .catch(err => res.status(400).json('unable to register'))
})

app.get('/profile/:id', (req, res) => {
  const { id } = req.params;
  db.select('*').from('users').where({id})
    .then(user => {
      if (user.length) {
        res.json(user[0])
      } else {
        res.status(400).json('Not found')
      }
  })
  .catch(err => res.status(400).json('error getting user'))
})

app.put('/image', (req, res) => {
  const { id } = req.body;
  db('users').where('id', '=', id)
  .increment('entries', 1)
  .returning('entries')
  .then(entries => {
    res.json(entries[0]);
  })
  .catch(err => res.status(400).json('unable to get entries'))
})

app.listen(4000, ()=> {
  console.log('app is running on port 4000');
});

```

---

## 304: Putting It All Together

### signIn.js

```jsx
import React from 'react';

class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      signInEmail: '',
      signInPassword: ''
    }
  }


  onEmailChange = (event) => {
    this.setState({signInEmail: event.target.value})
  }

  onPasswordChange = (event) => {
    this.setState({signInPassword: event.target.value})
  }

  onSubmitSignIn= () => {
    fetch('http://localhost:4000/signin', {
      method: 'post',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify({
        email: this.state.signInEmail,
        password: this.state.signInPassword
      })
    }).then(response => response.json())
      .then(user => {
        if(user.id){
          this.props.loadUser(user);
          this.props.onRouteChange('home');
        }
      })
  }

  render() {
    const { onRouteChange } = this.props;
    return (
    <article className="br3 ba b--black-10 mv4 w-100 w-50-m w-25-1 mw6 shadow-5 center">
      <main className="pa4 black-80">
      <div className="measure">
        <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
          <legend className="f1 fw6 ph0 mh0">Sign In</legend>
          <div className="mt3">
            <label className="db fw6 lh-copy f6" htmlFor="email-address">Email</label>
            <input
              className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
              type="email"
              name="email-address"  
              id="email-address"
              onChange={this.onEmailChange} />
          </div>
          <div className="mv3">
            <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
            <input
            className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
            type="password"
            name="password"  
            id="password"
            onChange={this.onPasswordChange} />
          </div>
        </fieldset>
        <div className="">
          <input
            onClick={this.onSubmitSignIn}
            className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib"
            type="submit"
            value="Sign in"
            />
        </div>
          <div className="lh-copy mt3">
            <p onClick={() => onRouteChange('register')} className="f6 link dim black db pointer">Register</p>
          </div>
        </div>
      </main>
    </article>
  );
  }
}

export default SignIn;
```

---

## 305: What's Next?

---
