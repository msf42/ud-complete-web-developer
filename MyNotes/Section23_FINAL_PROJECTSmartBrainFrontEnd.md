# Section 23: FINAL PROJECT: SmartBrain FrontEnd

## 244: What are We Building?

[Clarifai](https://www.clarifai.com)

---

## 245: Building Our Components

Basic setup

---

## 246: Note: Updated Clarifai API: FACE_DETECT_MODEL

Just a note

---

## 247: Image Recognition API

Connected to the API. Image is now displaying and info is getting returned from API.

---

## 248: Optional: Advanced `setState()`

[setState in React](https://reactjs.org/docs/react-component.html#setstate)

---

## 249: Face Detection Box

Done

---

## 250: Sign-In Form and Routing

Done

---

## 251: Project Files: Github

---
