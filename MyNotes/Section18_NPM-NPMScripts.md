# Section 18: NPM + NPM Scripts

## 188: Introduction to NPM

### Node Package Manager

Although it is now used for all JS

### Three Requirements for Beginning Every Project

1. HTML, CSS, JS
2. Git and Github Repo
3. Package.json file

---

## 189: Setting up NPM & package.json

All set up. Upgraded NPM with `npm install npm@latest -g`

---

## 190: Update: Latest Node.js and NPM

Done.

---

## 191: Troubleshoot: Installing NPM & Node.js

Not necessary

---

## 192: Installing & Using Packages

### [lodash](https://lodash.com)

- [lodash package on npm](https://www.npmjs.com/package/lodash)

### live-server

Refreshes and loads page automatically

Really nice lesson. Nothing to really take notes about - lots on npm.

---

## 193: Why Update Packages?

Done.

---

## 194: Quick Note about Packages

Done

---
