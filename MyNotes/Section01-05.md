# Sections 01 - 05

## Section 01: Introduction

---

### Lessons 01 - 04: Course Overview

---

---

## Section 02: How the Internet Works

---

### Lesson 05: Browsing the web

Three main parts to a website:

- HTML
- CSS
- JavaScript

---

### Lesson 06 and 07: Breaking Google

Using `Developer Tools` in a browser, you can modify the HTML of a webpage.

---

### Lesson 08: The Internet Backbone

Most internet traffic still travels through cables.

[Submarine Cable Map](https://www.submarinecablemap.com/) - the backbone of the internet

---

### Lesson 09 and 10: Traceroute

Using `traceroute` we can trace the path of a webpage - all the computers it travels through:

```bash
steve@steve-xubu:~$ traceroute google.com
traceroute to google.com (216.58.192.206), 30 hops max, 60 byte packets
 1  * * *
 2  96.120.108.53 (96.120.108.53)  41.527 ms  41.528 ms  45.726 ms
 3  xe-11-1-3-sur01.sharpsridge.tn.knox.comcast.net (69.139.213.33)  316.677 ms  316.674 ms  316.686 ms
 4  162.151.41.93 (162.151.41.93)  316.670 ms  316.648 ms  320.794 ms
 5  162.151.95.117 (162.151.95.117)  325.197 ms  336.990 ms  336.929 ms
 6  be-22909-cr02.350ecermak.il.ibone.comcast.net (68.86.92.5)  72.498 ms  48.072 ms  50.038 ms
 7  be-10577-pe03.350ecermak.il.ibone.comcast.net (68.86.86.2)  45.829 ms  47.759 ms  49.902 ms
 8  96-87-9-190-static.hfc.comcastbusiness.net (96.87.9.190)  345.629 ms 96-87-9-230-static.hfc.comcastbusiness.net (96.87.9.230)  349.742 ms 96-87-9-190-static.hfc.comcastbusiness.net (96.87.9.190)  354.747 ms
 9  * * *
10  108.170.238.138 (108.170.238.138)  351.493 ms 216.239.41.162 (216.239.41.162)  351.503 ms 72.14.232.168 (72.14.232.168)  351.420 ms
11  216.239.42.107 (216.239.42.107)  347.202 ms 108.170.243.254 (108.170.243.254)  342.914 ms 108.170.244.2 (108.170.244.2)  331.289 ms
12  216.239.63.33 (216.239.63.33)  333.045 ms ord30s25-in-f14.1e100.net (216.58.192.206)  52.119 ms 216.239.57.77 (216.239.57.77)  61.719 ms
```

---

### Lesson 11: Developer Fundamentals: I

Three things determine how a website loads:

- Distance
- How many trips are necessary
- Size of files

---

### Lesson 12: What Does a Developer Do?

![Full Stack Developer](./images/Lesson_14a.png)

---

### Lesson 13: The Article Which Inspired the Course

[Medium article](https://medium.com/zerotomastery/learn-to-code-in-2019-get-hired-and-have-fun-along-the-way-d4197f96be27)

---

---

## Section 03: History of the Web

---

### Lesson 14: WWW vs Internet

[Maps that explain the internet](https://www.vox.com/a/internet-maps) - Very cool!

The **internet** is a massive infrastructure network connecting computers around the world. The **WWW** is an information sharing protocol that allows computers to communicate across the internet.

---

### Lesson 15: HTML, CSS, Javascript

[First Web Page in the World](http://info.cern.ch/hypertext/WWW/TheProject.html)

Three main parts to a website:

- **HTML** came first - Words and hyperlinks
- **CSS** came next - Makes it pretty (colors, fonts, etc)
- **JavaScript** came third - Makes it interactive

---

### Lesson 16: Developer Fundamentals: II

While there are standards on how browsers should display pages, we still have to double check that pages look correct in all browsers and on mobile devices.

---

### Lesson 17: Developer History

These are the things we will learn:

![Modern Full Stack Developer](./images/Lesson_14b.png)

---

### Lesson 18: Exercise: Adding CSS and javascript to Tim's website

![The First Website](./images/Lesson_14c.png)

---

### Lesson 19: More About the History of the Web

YouTube Videos from *Crash Course in Computer Science*

- [Computer Networks](https://youtu.be/3QhU9jd03a0)
- [Internet](https://youtu.be/AEaKrq3SpW8)
- [World Wide Web](https://youtu.be/guvsH5OFizE)

---
---

## Section 04: HTML 5

---

### Lesson 20: Build Your First Website

#### Basic html:

```html
<!DOCTYPE html>
<html>
  <head>
    <title>My first website</title>
  </head>
  <body>
    Helloooo!!!
  </body>
</html>
```

#### Viewed in Browser

![Viewed in Browser](./images/21-index_html.png)

---

### Lesson 21: Your Text Editor

Atom, Sublime, VSC, etc

---

### Lesson 22: Developer Fundamentals: III

**Valuable website!** - [https://www.w3schools.com](https://www.w3schools.com)

 `<!DOCTYPE html>` says to browser: This document uses **HTML5**

---

### Lesson 23: Quick Note About w3schools

[MDN](https://developer.mozilla.org/en-US/) - another great resource, but we'll use it later

---

### Lesson 24: How to Ask Questions

1. [Rubber Duck Debugging](https://rubberduckdebugging.com)
2. [Stack Overflow](https://stackoverflow.com)
3. [Discord](https://www.discord.io)

---

### Lesson 25: HTML Tags

```html
<!DOCTYPE html>
<html>
  <head>
    <title>My first website</title>
  </head>
  <body>   <!--this is a tag-->
    <h1>BOOOOO</h1>  <!--this is an element-->
    <h2>BOOOOO</h2>
    <h6>BOOOOO</h6>

    <p><strong>Lorem ipsum</strong> dolor sit amet consectetur adipisicing elit. Cum nisi, distinctio amet voluptates odit possimus, incidunt praesentium nesciunt ad ullam quaerat pariatur maxime blanditiis est similique cumque itaque necessitatibus. Nobis!</p>
    <p><em>Lorem ipsum</em> dolor sit amet consectetur adipisicing elit. Cum nisi, distinctio amet voluptates odit possimus, incidunt praesentium nesciunt ad ullam quaerat pariatur maxime blanditiis est similique cumque itaque necessitatibus. Nobis!</p>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum nisi, distinctio amet voluptates odit possimus, incidunt praesentium nesciunt ad ullam quaerat pariatur maxime blanditiis est similique cumque itaque necessitatibus. Nobis!</p>

  </body>
</html>
```

#### Viewed in Browser

![Viewed in Browser](./images/26-index_html.png)

---

### Lesson 26: HTML Tags 2

#### Unordered Lists:

```html
<!DOCTYPE html>
<html>
  <head>
    <title>My first website</title>
  </head>
  <body>
    <ul>
      <li>banana</li>
      <li>apple</li>
        <li>fuji</li>
        <li>honecrisp</li>
      <li>orange</li>
    </ul>>
  </body>
</html>
```

#### Viewed in Browser

![Viewed in Browser](./images/27-index_html.png)

---

### Lesson 27: Self-Closing HTML Tags

Some tags are "self-closing" and don't require a closing tag, such as with images:

```html
<img src="web address" width=00 height=00>
```

#### index.html

```html
<!DOCTYPE html>
<html>
  <head>
    <title>My first website</title>
  </head>
  <body>
    <ul>
      <li>banana</li>
      <li>apple</li>
        <li>fuji</li><hr>
        <img src="https://s2uqdnlqz93lrjbq205ld0eu-wpengine.netdna-ssl.com/wp-content/uploads/2016/06/fuji-single-nosticker.png" width="42" height="42">
        <li>honey crisp</li>
      <li>orange</li>
    </ul>>
  </body>
</html>

<hr>

```

#### Browser View

![Browser View](./images/28-index_html.png)

---

### Lesson 28: Anchor Tag

Anchor tags provide links:

```html
<a href="page.html">Page Name</a>
```

#### index.html

```html
<!DOCTYPE html>
<html>
  <head>
    <title>My first website</title>
  </head>
  <body>
    <a href="newpage.html">New Page</a>
    <ul>
      <li>banana</li>
      <li>apple</li>
        <li>fuji</li><hr>
        <img src="https://s2uqdnlqz93lrjbq205ld0eu-wpengine.netdna-ssl.com/wp-content/uploads/2016/06/fuji-single-nosticker.png" width="42" height="42">
        <li>honey crisp</li>
      <li>orange</li>
    </ul>>
  </body>
</html>

<hr>
```

#### newpage.html

```html
<!DOCTYPE html>
<html>
  <head>
    <title>New Page</title>
  </head>
  <body>
    <h1>This is the new page!</h1>
    <a href="index.html">
      <h2>Go Back</h2>>
    </a>
  </body>
</html>

```

#### Browser View - index.html

![index.html](./images/29-index_html.png)

#### Browser View - newpage.html

![newpage.html](./images/29-newpage_html.png)

---

### Lesson 29: Q&A: index.html

Q: Why *index.html*?

A: It is the standard, and servers default to it for the main page.

---

### Lesson 30: Q&A: Relative vs Absolute Path

Q: What is the difference?

A: Absolute path can be used anywhere. Relative path can only be used locally.

---

---

## Section 05: Advanced HTML 5

---

### Lesson 31: HTML Forms

Only added a link to `index.html` for `register.html`

#### register.html

```html
<!DOCTYPE html>
<head>
  <title>Register</title>
</head>
<body>
  <form>
    First Name: <input type="text"><br>
    Last Name: <input type="text"><br>
    E-mail: <input type="email" required><br>
    Birthday: <input type="date"><br>
    Gender: <br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br>
    Pets: <br>
    <input type="checkbox"> Cat<br>
    <input type="checkbox"> Dog<br>
    <input type="submit" value="Register!">
    <input type="reset">
  </form>
</body>
</html>

```

#### Form from Code Above

![Form from code above](./images/32-register_html.png)

---

### Lesson 32: HTML Forms 2

#### register.html

```html
<!DOCTYPE html>
<head>
  <title>Register</title>
</head>
<body>
  <form>
    First Name: <input type="text"><br>
    Last Name: <input type="text"><br>
    E-mail: <input type="email" required><br>
    Password: <input type="password" min="5"><br>
    Birthday: <input type="date"><br>
    Gender: <br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br>
    Pets: <br>
    <input type="checkbox"> Cat<br>
    <input type="checkbox"> Dog<br>
    Cars: <br>
    <select multiple>
      <option value="volvo">Volvo</option>
      <option value="Audi">Audi</option>
    </select><br>
    <input type="submit" value="Register!">
    <input type="reset">
  </form>
</body>
</html>

```

#### Browser View

![register.html](./images/33-register_html.png)

---

### Lesson 33: Submitting a Form

Setting up the form to record information

#### register.html

```html
<!DOCTYPE html>
<head>
  <title>Register</title>
</head>
<body>
  <form method="GET" action>
    First Name: <input type="text" name="firstname"><br>
    Last Name: <input type="text" name="lastname"><br>
    E-mail: <input type="email" name="email" required><br>
    Password: <input type="password" name="password" min="5"><br>
    Birthday: <input type="date" name="birthday"><br>
    Gender: <br>
    <input type="radio" name="gender" value="male">Male<br>
    <input type="radio" name="gender" value="female">Female<br>
    <input type="radio" name="gender" value="other">Other<br>
    Pets: <br>
    <input type="checkbox" name="cat"> Cat<br>
    <input type="checkbox" name="dog"> Dog<br>
    Cars: <br>
    <select name="cars">
      <option value="volvo" name="volvo">Volvo</option>
      <option value="Audi" name="audi">Audi</option>
    </select><br>
    <input type="submit" value="Register!">
    <input type="reset">
  </form>
</body>
</html>
```

#### Browser View

![register.html](./images/34-register_html.png)

After submitting, the text below was appended to the url address:

```html
?                    <!--I added the line breaks for clarity-->
firstname=Steve
& lastname=Furches
& email=furches%40gmail.com
& password=Elvis.123
& birthday=2018-10-02
& gender=other
& cat=on
& cars=Audi
```

---

### Lesson 34: HTML Tags 3

- `<div></div>` is a block tag that allows us to divide up sections. Often used with CSS, to only apply features to a particular section.
- `<span></span>` is used similarly, but within a single line.
- `<!--This is a comment-->` In the text editor, CTRL+/ can be used as a shortcut.

---

### Lesson 35: HTML vs HTML 5

![36a](./images/Lesson_36a.png)

[HTML5 on W3schools](https://www.w3schools.com/html/html5_semantic_elements.asp)

#### Example of HTML5

```html
  <header>
    <h1>Register</h1>
  </header>
  <nav>
    <a href="www.google.com">Google</a>
  </nav>
  <footer>
    <p>Website made using VSCode</p>
  </footer>
```

#### Example from code above

![Example HTML above](./images/36-example_html.png)

---

### Lesson 36: Copy a Website

This lesson simply showed how to copy a website by clicking "view source" to view the HTML, then copying and pasting in a text file.

---

### Lesson 37: HTML Challenge

#### Challenge: Build a simple website.

I kept it pretty simple:

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Steve's Running Site</title>
</head>
<body>
  <h1>Welcome to Steve's Running Page</h1>
  <hr>
  <h2>Here are some of my running goals:</h2>
  <ul>
    <li>Run a third marathon in 2019</li>
    <li>Run a 10K in less than 50 minutes</li>
    <li>Run a new PR for 5K</li>
  </ul>
  <hr>
  <h2>Here are some of my favorite running websites:</h2> <br>
  <a href="www.runnersworld.com">Runner's World</a><br><br>
  <a href="www.marathonguide.com">Marathon Guide</a>
</body>
</html>
```

#### Browser View

![My First Site](./images/38-mysite_html.png)

---

### Lesson 38: HTML Lesson Files

Just includes the three HTML files from this section.

---

### Lesson 39: HTML Quiz

[Quiz Link](https://www.w3schools.com/html/html_quiz.asp)

---

### Lesson 40: Optional Exercise: HTML

[Free Code Camp HTML](https://learn.freecodecamp.org/responsive-web-design/basic-html-and-html5/)

---
