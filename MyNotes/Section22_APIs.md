# Section 22: APIs

## 240: Introduction to APIs

General Intro

---

## 241: Exercise: Web App Using Star Wars API

```js
fetch('https://swapi.co/api/people/7')
  .then(response => {
    return response.json()
  })
  .then(data => {
    console.log(data.name)
  })
  .catch(err => {
    log('Request failed', error)
  })
```

---

## 242: Optional Exercise: Speech Recognition

Nope.

---

## 243: Resources: Public APIs

[Public APIs](https://public-apis.xyz/)

---
