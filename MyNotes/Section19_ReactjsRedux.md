# Section 19: Reactjs and Redux

## 195: Introduction to React.js

Nice intro.

---

## 196: Quick Note: CRA Growing Pains

CRA 3 is out.

---

## 197: `create-react-app`

Set up React.js

---

## 198: `create-react-app` 2

Section on updating create-react-app. I already have version 3

---

## 199: `create-react-app` 3

Just an update.

---

## 200: Quick Note: Class vs Functional App.js

A quick heads up about the next lecture! Create React App 3 was just released and although the changes are minor compared to the previous video (so what you learned up until now is still valid), there is a small change in the next video that you will notice:

When we go into App.js file in the next video, instead of seeing this:

```js
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
          Learn React
          </a>
        </header>
      </div>
    );
  }
```

You may see this instead:

```js
function App() => {
  return (
    <div className="App">
      <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                    Edit <code>src/App.js</code> and save to reload.
                </p>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                    >
                    Learn React
                </a>
            </header>
        </div>
    );  
```

- Don't worry about the difference. We actually cover the topic later on in this section when we talk about class components and functional components. For now, just change App.js to use the Class syntax :). If you are really curious you can even see the code change that was made by the creators on github** [**here**](https://github.com/facebook/create-react-app/commit/e4fdac2418dbd1b3d51c8f0fecb474caa001f649#diff-9e2ecb03a0ec4155fbe42095c297a58e).
- PS:  I will be making an updated Create React App 3 video shortly. Stay tuned!

---

## 201: Code from Previous Video

```js
import React from 'react';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
```

---

## 202: Your First React Component

Not a lot to make notes on. Set up first React component. Here is current display:

![image](./images/s_E03D1AE5D22CFE3637299F232626AE7F800F4060B8CA19C8CA2BB252E62EEBCF_1560297518353_image.png)

### Hello.js

```js
import React, { Component } from 'react';
import './Hello.css';

const Hello = (props) => {
    return (
        <div className='f1 tc'>
            <h1>Hello World</h1>
            <p>{props.greeting}</p>  { /* comes from prop passed in index.js */}
        </div>
    );
}

// Either of these works - below is a React Component, above is JSX.

// class Hello extends Component {
//     render() {
//         return (
//             <div className='f1 tc'>
//                 <h1>Hello World</h1>
//                 <p>{this.props.greeting}</p>  
//             </div>
//         );
//     }
// }

export default Hello;
```

### index.js

```js
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Hello from './Hello';
import * as serviceWorker from './serviceWorker';
import 'tachyons'; // allows for shortened 'css' in React

// greeting prop gets passed in here
ReactDOM.render(<Hello greeting={'Hello' + 'React Ninja'}/>, document.getElementById('root'));

serviceWorker.unregister();
```

### Hello.css

```css
h1 {
    background: red;
}
```

---

## 203: Building a React App 1

### index.js

```js
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Card from './Card.js';
import * as serviceWorker from './serviceWorker';
import 'tachyons';
import { robots } from './robots';
// Since export const robots [ is at the top, rather than export default robots at the bottom, we import with { }
ReactDOM.render( // tells the DOM to show this
          <div> // a div with three sections. note the < />
              <Card id={robots[0].id} name={robots[0].name} email={robots[0].email}/>
               // from card, take first robots id, name, email
              <Card id={robots[1].id} name={robots[1].name} email={robots[1].email}/>
              <Card id={robots[2].id} name={robots[2].name} email={robots[2].email}/>
          </div>
, document.getElementById('root')); // from 'root' element
serviceWorker.unregister();
```

### card.js

```js
import React from 'react'; // required
// Card is a function with three elements - name, email, id
// tachyons allow the shortcuts in className - just CSS shortcuts
const Card = ({name, email, id}) => {
    return ( // everything has to be returned in one block between ()
        <div className='tc bg-light-green dib br3 pa3 ma2 grow bw2 shadow-5'>
            <img alt="robots" src={`https://robohash.org/${id}?200x200`} />
            // image from website, inserts id into url
            <div> // div below card with name and email
                <h2>{name}</h2>
                <p>{email}</p>
            </div>
        </div>
    )
}
export default Card;
```

### robot.js - this just contains the robot information

```js
export const robots = [ // when this is at the top, instead of 'export default' at the bottom, we can have multiple const - robots, cats, etc
    {
      id: 1,
      name: 'Leanne Graham',
      username: 'Bret',
      email: 'Sincere@april.biz'
    },
    {
      id: 2,
      name: 'Ervin Howell',
      username: 'Antonette',
      email: 'Shanna@melissa.tv'
    },
    {
      id: 3,
      name: 'Clementine Bauch',
      username: 'Samantha',
      email: 'Nathan@yesenia.net'
    },
    {
      id: 4,
      name: 'Patricia Lebsack',
      username: 'Karianne',
      email: 'Julianne.OConner@kory.org'
    },
    {
      id: 5,
      name: 'Chelsey Dietrich',
      username: 'Kamren',
      email: 'Lucio_Hettinger@annie.ca'
    },
    {
      id: 6,
      name: 'Mrs. Dennis Schulist',
      username: 'Leopoldo_Corkery',
      email: 'Karley_Dach@jasper.info'
    },
    {
      id: 7,
      name: 'Kurtis Weissnat',
      username: 'Elwyn.Skiles',
      email: 'Telly.Hoeger@billy.biz'
    },
    {
      id: 8,
      name: 'Nicholas Runolfsdottir V',
      username: 'Maxime_Nienow',
      email: 'Sherwood@rosamond.me'
    },
    {
      id: 9,
      name: 'Glenna Reichert',
      username: 'Delphine',
      email: 'Chaim_McDermott@dana.io'
    },
    {
      id: 10,
      name: 'Clementina DuBuque',
      username: 'Moriah.Stanton',
      email: 'Rey.Padberg@karina.biz'
    }
  ];
```

---

## 204: React.Fragment and Semantic HTML

Just some notes on the new `<fragment>` tag

---

## 205: Building a React App 2

### index.js

```js
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import CardList from './CardList';
import * as serviceWorker from './serviceWorker';
import 'tachyons';
import { robots } from './robots';

ReactDOM.render(
                <CardList robots={robots}/>
, document.getElementById('root'));

serviceWorker.unregister();

```

### CardList.js

```js
import React from 'react';
import Card from './Card';

const CardList = ({ robots }) => {
  return (
    <div>
      {
        robots.map((user, i) => {
          return (
            <Card
              key={"id" + Math.random().toString(16).slice(2)}
              id={robots[i].id}
              name={robots[i].name}
              email={robots[i].email}
            />
          );
        })
      }
    </div>
  );
}

export default CardList;
```

### Card.js

```js
import React from 'react';

const Card = ({ name, email, id }) => {
  return (
    <div className='tc bg-light-green dib br3 pa3 ma2 grow bw2 shadow-5'>
      <img alt="robots" src={`https://robohash.org/${id}?200x200`} />
      <div>
        <h2>{name}</h2>
        <p>{email}</p>
      </div>
    </div>
  );
}

export default Card;
```

---

## 206: Building a React App 3

### index.js

```js
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as registerServiceWorker from './registerServiceWorker';
import 'tachyons';
import { robots } from './robots';
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker.unregister();
```

### app.js

```js
import React, { Component } from 'react';
import CardList from './CardList';
import SearchBox from './searchBox';
import { robots } from './robots';
import { createWriteStream } from 'fs';

class App extends Component {
  constructor() {
    super()
      this.state = {
        robots: robots,
        searchfield: ''
    }
  }

onSearchChange= (event) => {
    this.setState({ searchfield: event.target.value })
}

    render() {
        const filteredRobots = this.state.robots.filter(robots => {
            return robots.name.toLowerCase().includes(this.state.searchfield.toLowerCase());
        })
        return (
            <div className='tc'>
                <h1>RoboFriends</h1>
                <SearchBox searchChange={this.onSearchChange}/>
                <CardList robots={filteredRobots}/>
            </div>
        );
    }
}
export default App;
```

A STATE can change, unlike a Property. A parent passes the STATE to a child, but it becomes a property at that point, and can not be changed.

### searchBox.js

```js
import React from 'react';
const SearchBox = ({ searchfield, searchChange}) => {
    return (
        <div className='pa2'>
            <input
            className='pa3 ba b--green bg-lightest-blue'
            type='search'
            placeholder='search robots'
            onChange={searchChange}
            />
        </div>
    );
}
export default SearchBox;
```

---

## 207: Styling Your React App

Just downloading fonts and adding them, nothing major.

---

## 208: Building a React App 4

Smart components have *state*

- [MDG API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)

- [reactjs.org](https://reactjs.org/docs/react-component.html)

### App.js

```js
import React, { Component } from 'react';
import CardList from './CardList';
import SearchBox from './SearchBox';
import './App.css';

class App extends Component {
  constructor() {
    super()
    this.state = {
      robots: [],
      searchfield: ''
    }
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response=> response.json())
      .then(users => this.setState({ robots: users }));
  }

  onSearchChange = (event) => {
    this.setState({ searchfield: event.target.value })
  }

  render () {
    const filteredRobots = this.state.robots.filter(robots => {
      return robots.name.toLowerCase().includes(this.state.searchfield.toLowerCase())
    })
    if (this.state.robots.lentgh === 0) {
      return <h1>Loading</h1>
    } else {
      return (
        <div className='tc'>
          <h1 className='f1'>RoboFriends</h1>
          <SearchBox searchChange={this.onSearchChange}/>
          <CardList robots={filteredRobots}/>
        </div>
      );
    }
  }
}
export default App;
```

---

## 209: Building a React App 5

### App.js - placed CardList within `<Scroll>`

```js
import React, { Component } from 'react';
import CardList from './CardList';
import SearchBox from './SearchBox';
import Scroll from './Scroll';
import './App.css';

class App extends Component {
  constructor() {
    super()
    this.state = {
      robots: [],
      searchfield: ''
    }
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response=> response.json())
      .then(users => this.setState({ robots: users }));
  }

  onSearchChange = (event) => {
    this.setState({ searchfield: event.target.value })
  }

  render () {
    const filteredRobots = this.state.robots.filter(robots => {
      return robots.name.toLowerCase().includes(this.state.searchfield.toLowerCase())
    })
    if (this.state.robots.lentgh === 0) {
      return <h1>Loading</h1>
    } else {
      return (
        <div className='tc'>
          <h1 className='f1'>RoboFriends</h1>
          <SearchBox searchChange={this.onSearchChange}/>
          <Scroll>
            <CardList robots={filteredRobots}/>
          </Scroll>
        </div>
      );
    }
  }
}
export default App;
```

### Scroll.js

```js
import React from 'react';
const Scroll = (props) => {
  return (
    <div style={{overflowY: 'scroll', border: '5px solid black', height: '800px'}}>
      {props.children}
    </div>
  )
}
export default Scroll;
```

---

## 210: Building a React App 6

Organized folders

Replaced if/else statement with highlighted
If robots.length is false (0), then “Loading”, otherwise,

```js
return !robots.length ?
  <h1>Loading</h1> :
  (
  <div className='tc'>
      <h1 className='f1'>RoboFriends</h1>
      <SearchBox searchChange={this.onSearchChange}/>
      <Scroll>
          <CardList robots={filteredRobots}/>
      </Scroll>
  </div>
  );
```

---

## 211: Project Files

[robofriends](https://github.com/aneagoie/robofriends)

---

## 212: Keeping Your Projects Up to Date

`npm audit` will give details about errors and warnings for package versions, while `npm audit fix` will fix them.

`npm update` will change versions to newest

---

## 213: React Review

Done

---

## 214: Error Boundary in React

```js
import React, { Component } from 'react';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false
    }
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true })
  }

  render() {
    if (this.state.hasError) {
      return <h1>Oooops. That is not good!</h1>
    }
  return this.props.children
  }
}

export default ErrorBoundary;
```

---

## **These are optional**

---

## 215: Deploying Our React App

---

## 216: Quick Note About Redux

---

## 217: State Management

---

## 218: Why Redux?

---

## 219: Installing Redux

---

## 220: Redux Actions and Reducers

---

## 221: Redux Store and Provider

---

## 222: Redux connect()

---

## 223: Redux Middleware

---

## 224: Redux Async Actions

---

## 225: Redux Project Structures

---

## 226: Popular Tool for React & Redux

---

## 227: Project Files - Redux

---
