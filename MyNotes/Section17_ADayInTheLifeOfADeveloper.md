# Section 17: A Day in the Life of a Developer

## 185: A Typical Day

Done.

---

## 186: Exercise: Helping a Developer

Looked through questions. Didn’t find an unanswered one that I could help with, but I will keep looking.

---

## 187: A Developer’s Morning Routine

[Hacker News](https://news.ycombinator.com)

[Software Engineering Daily](https://softwareengineeringdaily.com)

---
