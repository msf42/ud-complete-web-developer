# Sections 06 - 08

## Section 06: CSS

### Lesson 41: Exercise Files: Code Along

This simply included the three files for this section: `index.html`, `about.html`, and `login.html`

---

### Lesson 42: Your First CSS

#### The basics of CSS

```css
Selector {
  Property: value;
}
```

The typical way of adding CSS. Do it by adding this line in your header:

```html
<link rel="stylesheet" type="text/css" href="style.css">
```

A second way, inline styling:

```html
<header style="background-color: green; color:red">
```

Lastly, adding a style sheet to the `<head>` of the `html` file:

```css
  <style>
  li {
    background-color: blue
  }
  </style>
```

---

### Lesson 43: CSS Properties

Valuable Resource: [CSS Tricks](https://css-tricks.com)

```css
body {
  background-image: url(background_image.jpg);
  background-size: cover;                        /* This adjusts size */
}

h2 {
  color: #AA4139;                              /* hex and rgb are fine */
  text-align: center;
  border: 5px solid rgba(255, 176, 170, 0.5);  /* a and 0.5 are opacity */
  cursor: pointer;
}

p {
  color: pink;
}

li {
  list-style: none;
  display: inline-block;
}
```

---

### Lesson 44 and 45: CSS Selectors

#### CSS Cheat Sheet

Reference:

- [w3schools](https://www.w3schools.com/cssref/css_selectors.asp)
- [css-tricks](https://css-tricks.com/almanac/)

Cascading Style Sheets at the most basic level it indicates that the **order** of CSS rules matter.

```css
/* order of CSS */

/* 1st = class */
.class  /* can be used repeatedly */

/* 2nd = ID */
#id     /* can only be used once */

/* 3rd = star*/
*       /* usually at top, applies to everything */

/* 4th = element*/
h2      /* like we've already done */

/* 5th = element, element */
h2, p  /* applies to both */

/* 6th = element element */
h2 p   /* only applies to all p that are inside of h2 */

/* 7th = element > element */
h2 > p /* only applies to p that have a parent of h2 */

/* 8th = element + element */
h2 + p /* only applies to p right after h2

/* 9th = hover */
:hover /* only applies on hover, added after an element, e.g. p:hover */

/* 10th = last child */
:last-child /* only applies to last child in a set */

/* 11th = first child */
:first-child /* only applies to first child in a set */

/* 12th = important */
!important /* overrides others; not recommended because it breaks the rules */
```

What selectors win out in the cascade depends on:

- Specificity
  - The more specific something is, the more likely it will be effective
  - [this site](http://specificity.keegan.st) has a great calculator
- Importance - as in
  `!important`
- Source Order - as covered above

---

### Lesson 46: Text and Font

[Google fonts](https://fonts.google.com) - Use fonts from here for free.

Add this to the `<head>` tag of the `html` file:

```html
<link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
```

#### style.css

```css
* {
  text-align: right;
}

body {
  background-image: url(background_image.jpg);
  background-size: cover;  /* this adjusts size */
}

h2 + p {
  color: #AA4139;
  text-align: center;
  border: 5px solid rgba(111, 176, 170, 0.5); /* 'a' and 4th number are opacity */
  cursor: pointer;
}

p {
  /* text-decoration: line-through; */
  /* text-transform: uppercase; */
  line-height: 50px;
  font-style: italic;
  font-weight: bold;
  font-size: 180%;
  font-family: 'Poiret One', cursive;
}

li {
  list-style: none;
  display: inline-block;
}

.webtext {  /* a class */
  border: 5px dashed green;
}

.active {  /* another class */
  color: red;
}

#div1 { /* uses # sign. Can only be used once */
  background: rgba(0, 255, 0, 0.2);
}

#div2 {
  background: rgba(100, 0, 100, 0.3)
}
```

![image](images/46-CSS-index_html_style_css.png)

---

### Lesson 47: Images in CSS

First, place the image in with `html` in the appropriate location:

```html
<img src="http://www.scottishbooktrust.com/files/image_by_adam_rifkin_on_flickr_0.jpg" width="240px" height="200px">
```

Then, use `img` in CSS to designate location of image. Also use `clear` to make next section start below image. "both" clears both sides

```css
img {
  float: right; /* use sparingly, if it all */
}

footer {
  clear: both;
  text-align: center;
}
```

---

### Lesson 48: Box Model

![box model](./images/boxmodel6.svg)

```css
.boxmodel {
  border: 5px solid red;
  display: inline-block;
  padding: 5px 20px 5px 20px; /* top right bottom left */
  margin: 0px 20px; /* top and bottom; left and right */
  width: 33 px;
  height: 55 px;
}
```

```html
  <div class="boxmodel">A</div>
  <div class="boxmodel">B</div>
  <div class="boxmodel">C</div>
```

#### login.html with boxmodel added to CSS

![login.html with boxmodel added to CSS](./images/box.jpeg)

---

### Lesson 49: px vs em vs rem

#### login.html

```html
<!DOCTYPE html>
<html>
<head>
  <title>CSS</title>
  <link rel="stylesheet" type="text/css" href="style2.css">
</head>
<body>
  <header>
  <nav>
    <ul>
    <li><a href="index.html">Home</a></li>
    <li><a href="about.html">About</a></li>
    <li>Login</li>
    </ul>
  </nav>
  </header>
  <section>
  <h2>Login</h2>
  <p>Introduction</p>
  <p><span>Lorem ipsum</span> dolor sit amet, consectetur adipisicing elit.
    Nisi libero ex minima, dolores esse non voluptas hic veritatis
    officia asperiores fugiat quod velit similique atque ut dolorem,
    in iste? Fuga.
  </p>
  </section>
</body>
</html>
```

#### style2.css

```css
h2 {
  color: blue;
}
p {
  font-size: 20px;
}
span {
  font-size: 5em; /* this means 5X relative to containing element */
  /* can also use rem, which is relative to whole doc */
}
```

![image](./images/49-CSS-login_html_style2_css.png)

---

### Lesson 50: CSS Quiz

[Quiz Link](https://www.w3schools.com/css/css_quiz.asp)

---

---

## Section 07: Advanced CSS

---

### Lesson 51: Critical Render Path

**Critical Render Path:** The path that the website must take in order to render the website.
A webpage can not be rendered until the browser has received the CSS.
The font files also have to be loaded.

[Minify Your CSS](https://www.cleancss.com/css-minify/)

It strips white space and shrinks the file, to make it load faster.

---

### Lesson 52: Exercise File: Code-Along Images

#### Images

![image](./images/horse-herd-fog-nature-52500.jpeg)

---

![image](./images/elephant-cub-tsavo-kenya-66898.jpeg)

---

![image](./images/pexels-photo-213399.jpeg)

---

![image](./images/ibis-bird-red-animals-158471.jpeg)

---

![image](./images/pexels-photo-133459.jpeg)

---

![image](./images/ape-berber-monkeys-mammal-affchen-50988.jpeg)

---

### Lesson 53: Flexbox

Flexbox is a great way to display pictures

#### index.html

```html
<html>
<head>
  <title>CSS</title>
  <link rel="stylesheet" type="text/css" href="style.css">
  <meta content="">
  <style></style>
</head>
<body>
  <h1>Life in the Wild</h1>
  <div class="container">
  <img src="https://static.pexels.com/photos/52500/horse-herd-fog-nature-52500.jpeg">
  <img src="https://static.pexels.com/photos/66898/elephant-cub-tsavo-kenya-66898.jpeg">
  <img src="https://static.pexels.com/photos/213399/pexels-photo-213399.jpeg">
  <img src="https://static.pexels.com/photos/158471/ibis-bird-red-animals-158471.jpeg">
  <img src="https://static.pexels.com/photos/133459/pexels-photo-133459.jpeg">
  <img src="https://static.pexels.com/photos/50988/ape-berber-monkeys-mammal-affchen-50988.jpeg">
  </div>
</body>
</html>
```

#### style.css

```css
.container {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}
h1 {
  font-family: fantasy;
  font-size: 3em;
  border-bottom: 2px solid pink;
  border-right: 2px solid pink;
  width: 400px;
  text-align: center;
}
img {
  width: 450px;
  height: 350px;
  margin: 10px
}
```

#### Displayed in browser

![image](./images/52-FLEXBOX-index_html_style_css-1570565553631.png)

---

### Lesson 54: Flexbox Froggy

[Cheatsheet](https://darekkay.com/dev/flexbox-cheatsheet.html)

[Flexbox Froggy](http://flexboxfroggy.com)

This was actually a very helpful game. I completed all 24 levels

---

### Lesson 55: CSS3

Some newer features may require a browser prefix.  Check [w3schools Browser Support](https://www.w3schools.com/cssref/css3_browsersupport.asp) to find out.

Another valuable website is [caniuse.com](https://caniuse.com)

#### style.css

```css
body {
  margin: 0px;
}
.container {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}
h1 {
  font-family: fantasy;
  font-size: 3em;
  border-bottom: 2px solid pink;
  border-right: 2px solid pink;
  width: 400px;
  text-align: center;
  box-shadow: 4px 4px 5px #888888;
  margin-top: 10px;
}
img {
  width: 450px;
  height: 350px;
  margin: 10px;
  transition: all 1s;  /* 1 second for transitions that follow */
}
img:hover {  /* this is a transition */
  transform: scale(1.1);
}
```

---

### Lesson 56: Optional Exercise: Mastering Transitions and Transforms

[Website](https://robots.thoughtbot.com/transitions-and-transforms)

---

### Lesson 57: Responsive UI

Responsive UI means making sure a site looks good on any device. Using the Developer tab we can check in a browser:

![image](./images/57-FLEXBOX-index_html_style_css-1570565594017.png)

---

### Lesson 58: Image Gallery Files

- [index.html](../LessonFiles/058/index.html)
- [style.css](../LessonFiles/058/style.css)

---

### Lesson 59-61: Robot Animation

- [index-problem.html](../LessonFiles/059-061/index-problem.html)
- [index-answer.html](../LessonFiles/059-061/index-answer.html)
- [style-problem.css](../LessonFiles/059-061/style-problem.css)
- [style-answer.css](../LessonFiles/059-061/style-answer.css)

---

### Lesson 62: Optional Exercise: CSS

The Free Code Camp CSS course, which I was already working on:

[fCC Basic CSS](https://learn.freecodecamp.org/responsive-web-design/basic-css/)

---

---

## Section 08: Bootstrap 4, Templates, & Building Your Startup Landing Page

---

### Lesson 63: Bootstrap Introduction

- [Bootstrap](https://getbootstrap.com)
- [Foundation](https://foundation.zurb.com)

These sites, especially Bootstrap, are great for getting pre-made pieces of code.

---

### Lesson 64: Bootstrap 4

#### bootstrap/index.html

All of this content came from Bootstrap

```html
<!DOCTYPE html>
<html>
  <head>
  <title>Bootstrap</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <!--above is a link to the bootstrap CSS. below is a link to mine-->
  <link rel="stylesheet" type="text/css" href="style.css"
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    <li class="nav-item active">
      <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Link</a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Dropdown
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      <a class="dropdown-item" href="#">Action</a>
      <a class="dropdown-item" href="#">Another action</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="#">Something else here</a>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
    </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    </div>
  </nav>
  <div class="jumbotron">
    <h1 class="display-4">Hello, world!</h1>
    <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
    <hr class="my-4">
    <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
    <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
  </div>
  <!-- Button trigger modal -->
  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>
    <div class="modal-body">
    ...
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">Save changes</button>
    </div>
  </div>
  </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
```

#### bootstrap/style.css

This is my CSS that I used to override the bootstrap CSS

```css
.btn-danger {
  background-color: orange;
}
.btn-danger:hover {
  background-color: black;
}
```

#### What is a CDN?

[Cloudflare](https://www.cloudflare.com/learning/cdn/what-is-a-cdn/)

![image](./images/64-BOOTSTRAP-index_html_style_css.png)

---

### Lesson 65: Bootstrap 4 Grid

Bootstrap has a built-in idea of a grid system with 12 lines. We can list different size columns within our `class` based on the size of the window. It will adjust as the window size adjusts.

#### bootstrap/index.html

```html
  <div class="container">
    <div class="row">
    <div class="col col-sm-6 col-md-12 col-lg-12">
    1 of 2
    </div>
    <div class="col col-sm-3 col-md-6 col-lg-12">
    2 of 2
    </div>
    <div class="col col-sm-3 col-md-6 col-lg-12">
    2 of 2
    </div>
    </dif>
    </div>
```

#### bootstrap/style.css

```css
.col {
  background-color: gray;
  border: 2px solid black;
}
```

---

### Lessons 66 - 70: Startup Landing Page

![header](./images/header.jpg)

- [index.html](../LessonFiles/066-070/index.html)
- [style.css](../LessonFiles/066-070/style.css)

---

### Lessons 71 - 72: Exercise: Adding Email Subscribe Form with Mailchimp

Used [www.mailchimp.com](http://www.mailchimp.com) to generate a form for collecting email addresses. Modified the form to fit the site. Links are above.

---

### Lesson 73: Putting Your Website Online

Downloaded desktop version of Github. Published website to my previous “hello world” page.

---

### Lesson 74: Note: Putting Your Website Online

No issues here.

---

### Lesson 75: Developer Fundamentals IV

General overview of concepts. Focused a lot on resources and the need to learn CSS.

[animante.css](https://daneden.github.io/animate.css)

[createive-tim.com](https://www.creative-tim.com/)

---

### Lesson 76: Templates

- [mashup-template.com](http://www.mashup-template.com/templates.html)
- [index.html](../LessonFiles/076/index.html)
- [style.css](../LessonFiles/076/style.css)

---

### Lesson 77: Resource for FREE Templates

Wow, you've learned a lot up to this point. Now with your new found knowledge you can build so many different websites! To help you with the process, I wanted to share with you some free templates that you can download right now and start customizing. Here are my favourite resources (some of which I have discussed in the previous video):

- New: [https://cruip.com/](https://cruip.com/) (This is the best)
- New: [Bootstrap Templates 0](https://mdbootstrap.com/freebies/)
- [Bootstrap Templates 1](http://mashup-template.com/templates.html)
- [Creative Tim Templates](https://www.creative-tim.com/bootstrap-themes/ui-kit?direction=asc&sort=price)
- [Bootstrap Templates 2](https://startbootstrap.com/template-categories/all/)
- [Animate.css](https://daneden.github.io/animate.css/)

---

### Lesson 78: Student Landing Pages

Just examples

---
