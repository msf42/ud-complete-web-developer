# Section 13: Advanced Javascript

## 133: Scope

- When working in a browser, you are working in the root (parent) scope.
- Functions have access to any variables in the root level.

### script.js

```javascript
//scope
// Root Scope (window)
var fun = 5;
function funFunction() {
    // child scope
    var fun = "hellooo";
    console.log(1, fun);
}
function funnerFunction() {
    // child scope
    var fun = "Byee";
    console.log(2, fun);
}
function funnestFunction() {
    // child scope
    fun = "AHHHH";
    console.log(3, fun);
}
console.log("window", fun); // returns window 5
funFunction();  // returns 1 fun
funnerFunction();  // returns 2 Byeee
funnestFunction();  // returns 3 AHHHH
```

---

## 134: Exercise: Scope

```javascript
// For all of these, what is the value of a when the function gets called with the alert()
// #1 ans: 3
function q1() {
    var a = 5;
    if(a > 1) {
        a = 3;
    }
    alert(a);
}
//#2 ans: 5
var a = 0;
function q2() {
    a = 5;
}
function q22() {
    alert(a);
}

//#3 ans: hello
function q3() {
    window.a = "hello";
}

function q32() {
    alert(a);
}
//#4 ans: test
var a = 1;
function q4() {
    var a = "test";
    alert(a);
}
//#5 ans: 5
var a = 2;
if (true) {
    var a = 5;
    alert(a);
}
alert(a);
```

---

## 135: Advanced Control Flow

### Ternary Operator

- Syntax: `Condition? expr1 : expr2`
- Great for checking a condition, and providing two options. Can often substitute for if/else statements.

```javascript
// condition ? expr1 : expr2
function isUserValid(bool) {
    return bool;
}
var answer = isUserValid(true) ? "You may enter" : "Access denied"
var automateAnswer =
    "Your account # is " + ( isUserValid(false) ? "1234" : "not available")
```

### Switch

- Switches are great substitutes for if/elif/elif/elif/else statements

```javascript
function moveCommand(direction) {
    var whatHappens;
    switch (direction) {
        case "forward":
            whatHappens = "you encounter a monster"
            break;
        case "back":
            whatHappens = "you arrive home"
            break;
        case "right":
            whatHappens = "you found a river"
            break;
        case "left":
            whatHappens = "you run into a troll"
            break;
        default:
            whatHappens = "pleas eenter a valid direction"
    }
    return whatHappens;
}
```

---

## 136: Exercise: Advanced Control Flow

```javascript
//#1 change this function into a ternary and assign it to variable called experiencePoints
function experiencePoints() {
    if (winBattle()) {
        return 10;
    } else {
        return 1;
    }
}
// ans
var experiencePoints = winBattle() ? 10 : 1;

//Using this function, answer the questions below:
function moveCommand(direction) {
    var whatHappens;
    switch (direction) {
        case "forward":
            break;
            whatHappens = "you encounter a monster";
        case "back":
            whatHappens = "you arrived home";
            break;
            break;
        case "right":
            return whatHappens = "you found a river";
            break;
        case "left":
            break;
            whatHappens = "you run into a troll";
            break;
        default:
            whatHappens = "please enter a valid direction";
    }
    return whatHappens;
}
//#2 return value when moveCommand("forward"); undefined
//#3 return value when moveCommand("back"); "you arrive home"
//#4 return value when moveCommand("right"); "you found a river"
//#5 return value when moveCommand("left"); "you arrive home"
//BONUS: practice makes perfect. Go and write your own switch function. It takes time to get used to the syntax!
```

---

## 137: ES5 and ES6

- ECMA International
- ECMAscript == Javascript
- ECMAscript Version 6 = ES6

- [BABEL](https://babeljs.io) allows you to write something in a newer version of JS, and it will convert it to an older version that ALL browsers know.

### Let & Const

```javascript
const player = 'bobby';
let experience = 100;
var wizardLevel = false;
if (experience > 90) {
    var wizardLevel = true;
    console.log('inside', wizardLevel);
}
console.log('outside', wizardLevel);
```

- Above returns inside true, outside true
- Below returns inside true, outside false

```javascript
const player = 'bobby';
let experience = 100;
let wizardLevel = false;
if (experience > 90) {
    let wizardLevel = true;
    console.log('inside', wizardLevel);
}
console.log('outside', wizardLevel);
```

If `let` is within `{}`, it creates a new *scope*. `var` does not.

**Do not use** `var` **anymore! Only use** `let`!

Constants can not be changed. You may even want to make a function a constant:

```javascript
const a = function() {
    console.log('a')
}
```

- So, use `const` for items that can’t be changed, use `let` for items that can.
- The `obj` constant can not be reassigned, but a property of it can.

```javascript
const obj = {
    player: 'bobby',
    experience: 100,
    wizardLevel: false
}

obj = 5
VM252:1 Uncaught TypeError: Assignment to constant variable.
    at <anonymous>:1:5
(anonymous) @ VM252:1

obj.experience = 99
99
```

### Destructuring

```javascript
const obj = {
    player: 'bobby',
    experience: 100,
    wizardLevel: false
}

// before, you would have to do this to access these properties
const player = obj.player;
const experience = obj.experience;
let wizardLevel = obj.wizardLevel;

// now we can do this
const { player, experience } = obj;
let { wizardLevel } = obj;

console.log(player) // => bobby
console.log(experience) // => 100
console.log(wizardLevel) // => false
```

```javascript
const name = 'john snow';
const obj = {
    [name]: 'hello',
    [1 + 2]: 'hihi'
}

obj // => {3: "hihi", john snow: "hello"}
```

### Const properties

```javascript
const a = "Simon";
const b = true;
const c = {};

// instead of
const obj = {
    a: a,
    b: b,
    c: c
}

// do this if property and value are same
const obj = {
    a,
    b,
    c
}
```

### Template Strings

```javascript
const name = "sally";
const age = 34;
const pet = "horse";

// old way
// const greeting = "Hello " + name + "you\'re ....."

const greetingBest = `Hello ${name} you seem to be ${age - 10}. What a lovely ${pet} you have.`
undefined
greetingBest
"Hello sally you seem to be 24. What a lovely horse you have."
```

### Default Arguments

```javascript
function greet(name='', age=30, pet='cat') {
    return `Hello ${name} you seem to be ${age - 10}. What a lovely ${pet} you have.`
}
undefined
greet()
"Hello  you seem to be 20. What a lovely cat you have."

greet('steve', 43, 'nose')
"Hello steve you seem to be 33. What a lovely nose you have."
```

### Symbol

```javascript
let sym1 = Symbol();
let sym2 = Symbol('foo');
let sym3 = Symbol('foo');
undefined
sym1
Symbol()
sym2
Symbol(foo)
sym3
Symbol(foo)
sym2 == sym3  // because symbols are all unique
false
```

### Arrow Functions

```javascript
function add(a, b) {
    return a + b;
}

// Instead, use this
const add = (a,b) => a + b;
```

---

## 138: Exercise: ES5 and ES6

```javascript
// change everything below to the newer Javascript!

// let + const
var a = 'test';
var b = true;
var c = 789;
a = 'test2';

// answer
let a = 'test';
const b = true;
const c = 789;
a = 'test2';

// ############################################
// Destructuring
var person = {
    firstName : "John",
    lastName  : "Doe",
    age       : 50,
    eyeColor  : "blue"
};

var firstName = person.firstName;
var lastName = person.lastName;
var age = person.age;
var eyeColor = person.eyeColor;

// answer
const person = {
    firstName : "John",
    lastName  : "Doe",
    age       : 50,
    eyeColor  : "blue"
};

const {age, firstName, lastName, eyeColor} = obj;

// ############################################
// Object properties
var a = 'test';
var b = true;
var c = 789;

var okObj = {
  a: a,
  b: b,
  c: c
};

// answer
const a = 'test';
const b = true;
const c = 789;

var okObj = {a, b, c};

// ############################################
// Template strings
var message = "Hello " + firstName + " have I met you before? I think we met in " + city + " last summer no???";

// answer
const message = `Hello ${firstName} have I met you before? I think we met in ${city} last summer no???`;

// ############################################
// default arguments
// default age to 10;
function isValidAge(age) {
    return age
}

// answer
function isValidAge(age = 10) {
    return age
}

// ############################################
// Symbol
// Create a symbol: "This is my first Symbol"
// answer
const sym1 = Symbol('This is my first symbol');

// ############################################
// Arrow functions
function whereAmI(username, location) {
    if (username && location) {
        return "I am not lost";
    } else {
        return "I am totally lost!";
    }
}

// answer
const whereAmI = (username, location) => {
    if (username && location) {
        return "I am not lost";
    } else {
        return "I am totally lost!";
    }
}
```

---

## 139: Advanced Functions

```javascript
const first = () => {
    const greet = 'Hi';
    const second = () => {
        alert(greet);
    }
    return second;
}
const newFunc = first();
newFunc();
//Closures - children always have access to parent scope
```

```javascript
//Currying - converting functions that take multiple arguments
// into those that take args one at a time
const multiply = (a, b) => a * b;
const curriedMultiply = (a) => (b) => a * b;
// running in console
multiply(3,4)
12
curriedMultiply(3)(4)
12

// Compose - the output of two functions are used in a third
const compose = (f, g) => (a) => f(g(a));
const sum = (num) => num + 1;
compose(sum, sum)(5);  // returns 7

// Avoiding Side Effects, Functional Purity (always returning)
// *Deterministic = same input always produces same output
```

---

## 140: Exercises: Advanced Functions

```javascript
//Solve these problems:

//#1 Create a one line function that adds adds two parameters
const add = (x, y) => x+y;

//Closure: What does the last line return?
const addTo = x => y => x + y
var addToTen = addTo(10)
addToTen(3) // 13

//Currying: What does the last line return?
const sum = (a, b) => a + b
const curriedSum = (a) => (b) => a + b
curriedSum(30)(1)  // 31


//Currying: What does the last line return?
const sum = (a, b) => a + b
const curriedSum = (a) => (b) => a + b
const add5 = curriedSum(5)
add5(12) // 17

//Composing: What does the last line return?
const compose = (f, g) => (a) => f(g(a));
const add1 = (num) => num + 1;
const add5 = (num) => num + 5;
compose(add1, add5)(10) // 16

//What are the two elements of a pure function?
// no side effects, determinate
```

---

## 141: Advanced Arrays

### Advanced Arrays

```javascript
const array = [1, 2, 10, 16];
const double = []
const newArray = array.forEach((num) => {
    double.push(num * 2);
});
console.log('forEach', double);
// output
//(4) [2, 4, 20, 32]
```

- Use map for simple loops
- Map always expects to return an element
- Map creates a new array
- Map reduces side effects

### `map`

```javascript
const mapArray = array.map((num) => {
    return num * 2
});
console.log('map', mapArray);
// output
//(4) [2, 4, 20, 32]

// can shorten to this if only one variable
const mapArray = array.map(num => num * 2);
```

### `filter`

```javascript
const filterArray = array.filter(num => {
    return num > 5
});

console.log('filter', filterArray);
// output
// filter (2) [10, 16]
```

### `reduce`

```javascript
const reduceArray = array.reduce((accumulator, num) => {
    return accumulator + num
}, 0);
console.log('reduce', reduceArray);
// output
// reduce 29
```

---

## 142: Exercise: Advanced Arrays

```javascript
// Complete the below questions using this array:
const array = [
  {
    username: "john",
    team: "red",
    score: 5,
    items: ["ball", "book", "pen"]
  },
  {
    username: "becky",
    team: "blue",
    score: 10,
    items: ["tape", "backpack", "pen"]
  },
  {
    username: "susy",
    team: "red",
    score: 55,
    items: ["ball", "eraser", "pen"]
  },
  {
    username: "tyson",
    team: "green",
    score: 1,
    items: ["book", "pen"]
  },

];

//Create an array using forEach that has all the usernames with a "!" to each of the usernames
let newArray = []
array.forEach(user => {
    let { username } = user;
    username = username + "!";
    newArray.push(username);
})
console.log(newArray);

//Create an array using map that has all the usernames with a "? to each of the usernames
const mapArray = array.map(user => {
  let { username } = user;
  return username + "?";
})
console.log(mapArray);

//Filter the array to only include users who are on team: red
const filterArray = array.filter(user => {
  return user.team === "red";
})
console.log(filterArray);

//Find out the total score of all users using reduce
const reduceArray = array.reduce((accumulator, user) => {
  return accumulator + user.score
} 0);
console.log(total);

// (1), what is the value of i? Index of the array
// (2), Make this map function pure:
const arrayNum = [1, 2, 4, 5, 8, 9];
const newArray = arrayNum.map((num, i) => {
    console.log(num, i);
    alert(num);
    return num * 2;
})

const arrayNum = [1, 2, 4, 5, 8, 9];
const newArray = arrayNum.map((num, i) => {
    return num * 2;
})

//BONUS: create a new list with all user information, but add "!" to the end of each items they own.
const answer = array.map(user => {
    user.items = user.items.map(item => {
        return item + "!"
    });
    return user;
})
console.log(answer);
```

---

## 143: Advanced Objects

![image](./images/s_FAECDB49EF29D21ADD9E5C332B3EBD7E7D22EDC5CFF96D9D0690BDE774508B24_1558222261654_image.png)

### Reference type

```javascript
var object1 = { value: 10 };
var object2 = object1;
var object3 = { value: 10 };
object1 === object2
object1 === object3
// numbers, booleans, strings, etc are all primitive types
// reference types simply point to a value
```

### Context vs Scope

```javascript
console.log(this); // your location - the object you are in
this.alert("hello"); // same as window.alert, because I am currently in the window
const object4 = {
    a: function() {
        console.log(this);
    }
} // now undefined
```

### Instantiation

```javascript
// when you make a copy of an object and reuse the code
class Player {
    constructor(name, type) {
        console.log('player', this);
        this.name = name;
        this.type = type;
    }
    introduce() {
        console.log(`Hi I am ${this.name}, I'm a ${this.type}`);
    }
}
class Wizard extends Player {
    constructor(name, type) {
        super(name, type);
        console.log('wizard', this);
    }
    play() {
        console.log(`WEEEE I'm a ${this.type}`);
    }
}
const wizard1 = new Wizard('Shelly', 'Healer');
const wizard2 = new Wizard('Shawn', 'Dark Magic');
```

---

## 144: Quick Note: Upcoming Videos

Next two are from Advanced JS course.

---

## 145: Pass by Value vs Pass by Reference

```javascript
var a = 5;
var b = a;
b++;
console.log(a) // output = 5
console.log(b) // output = 6
```

```javascript
let obj1 = {name: 'Yao', password:'123'};
let obj2 = obj1;
obj2.password = 'abc';
console.log(obj1); // output = abc
console.log(obj2); // output = abc
```

```javascript
var c = [1, 2, 3, 4, 5];
var d = c;
d.push(13848480);
console.log(d);  // output = [1,2,3,4,5,13848480]
console.log(c);  // output = [1,2,3,4,5,13848480]
var e = [].concat(c);
c.push(888);
console.log(e); // output = [1,2,3,4,5,13848480]
console.log(c); // output = [1,2,3,4,5,13848480,888]
```

```javascript
let obj = {a: 'a', b: 'b', c: 'c'};
let clone = Object.assign({}, obj);
obj.c = 5;
console.log(clone); // output: {a: "a", b: "b", c: "c"}
let clone2 ={...obj} // another way

let obj = {
    a: 'a',
    b: 'b',
    c: {
        deep: 'try and copy me'
    }
};
let clone = Object.assign({}, obj);
let clone2 = {...obj};
obj.c.deep = 'hahaha';
console.log(obj);
console.log(clone);
console.log(clone2);
// output of all 3 is
// {a: 'a', b: 'b', c: {deep: 'try and copy me'}}
// this is a 'shallow clone'
// the second address never changed

let superClone = JSON.parse(JSON.stringify(obj));
obj.c.deep = 'changed!';
console.log(obj); // still 'changed!'
console.log(superClone); // still 'hahaha'
```

---

## 146: Type Coercion

- [JS Equity Table](https://dorey.github.io/JavaScript-Equality-Table)
- [Comparisons and Sameness](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness)
- [ECMA](https://www.ecma-international.org/ecma-262/5.1/#sec-11.9.3)

```javascript
1 == '1';
true
// javascript will convert to 1 == 1
// Type Coercion only works with '==';
```

```javascript
1 === 1;
false
// === means explicit comparison
```

```javascript
if (1) {
    console.log(5)
}
// output: 5; JS coerces 1 to 'true'
```

---

## 147: Exercise: Advanced Objects

```javascript
//Evaluate these:
//#1
[2] === [2] // false
{} === {} // false
//#2 what is the value of property a for each object.
const object1 = { a: 5 };
const object2 = object1;
const object3 = object2;
const object4 = { a: 5};
object1.a = 4; // 4, 4, 4, 5

//#3 create two classes: an Animal class and a Mammal class.
// create a cow that accepts a name, type and color and has a sound method that moo's her name, type and color.
class Animal {
    constructor(name, type) {
        console.log('Animal', this);
        this.name = name;
        this.type = type;
    }
    introduce() {
        console.log(`Hi, I am a ${this.name}, and I am a ${this.type}`);
    }
}
const Animal1 = new Animal('Daisy', 'cow');
Animal1.introduce(); //Hi, I am a Daisy, and I am a cow

class Cow extends Animal {
    constructor(name, type, color, sound) {
        super(name, type);
        this.sound = sound;
        this.color = color;
        console.log('Cow', this);
    }
    intro2() {
        console.log(`Hi, I am ${this.name} the ${this.color} ${this.type} and I say ${this.sound}`);
    }
}
const Cow1 = new Cow('Daisy', 'dairy cow', 'brown', 'moo' );
Cow1.intro2(); //Hi, I am Daisy the brown dairy cow and I say moo
```

---

## 148: ES7

### `.includes()` and exponents

```javascript
'Hellooooooo'.includes('o');
// true
const pets = ['cat', 'dog', 'bat']
pets.includes('dog');
// true
pets.includes('bird');
// false

const square = (x) => x**2
square(7);
// 49
const cube = (x) => x**3
cube(3);
// 27
```

---

## 149: Exercise: ES7

```javascript
// #1) Check if this array includes the name "John".
const dragons = ['Tim', 'Johnathan', 'Sandy', 'Sarah'];
dragons.includes('John')
// false

// #2) Check if this array includes any name that has "John" inside of it. If it does, return that
// name or names in an array.
const dragons = ['Tim', 'Johnathan', 'Sandy', 'Sarah'];
dragons.filter(name => name.includes('John'))
//  [ 'Johnathan' ]

// #3) Create a function that calulates the power of 100 of a number entered as a parameter
const pow100 = (x) => x**100
pow100(2)
// 1.2676506002282294e+30

// #4) Using your function from #3, put in the paramter 10000. What is the result?
// Research for yourself why you get this result
// The result is 'infinity', because it essentially is.
```

---

## 150: ES8

```javascript
'Turtle'.padStart(10);
// "    Turtle"
'Turtle'.padEnd(10);
// "Turtle    "

//------------------

const fun = (a,b,c,d,) => {
    console.log(a);
}
fun(1,2,3,4,)
// 1
// This is because it is often easier to write large functions
// like this:
const fun = (
            a,
            b,
            c,
            d,
            ) => {
                console.log(a);
            }

//------------------

Object.keys
Object.values
Object.entries
let obj = {
    username0: 'Santa',
    username1: 'Rudolph',
    username2: 'Grinch'
}
// old way
Object.keys(obj).forEach((key, index) => {
    console.log(key, obj[key]);
});
// username0 Santa
// username1 Rudolph
// username2 Grinch

// This is the new way
Object.values(obj).forEach(value => {
    console.log(value);
});
// Santa
// Rudolph
// Grinch

Object.values(obj).forEach(entries => {
    console.log(entries);
});
//[ 'username0', 'Santa' ]
//[ 'username1', 'Rudolph' ]
//[ 'username2', 'Grinch' ]
Object.entries(obj).map(value=> {
    return value[1] + value[0].replace('username', '');
})
// [ 'Santa0', 'Rudolph1', 'Grinch2' ]
```

---

## 151: Exercise: ES8

```javascript
// #1) Line up the Turtle and the Rabbit at the start line:
const startLine = '     ||<- Start line';
let turtle = '🐢';
let rabbit = '🐇';
startLine
turtle = turtle.padStart(7);
rabbit = rabbit.padStart(7);
// it should look like this:
'     ||<- Start line'
'       🐢'
'       🐇'
// when you do:
console.log(startLine);
console.log(turtle);
console.log(rabbit);

// #2) What happens when you run turtle.trim().padEnd(9, '=') on the turtle variable
// Read about what the second parameter does in padEnd and padStart
turtle = turtle.trim().padEnd(9, '=');

// #3) Get the below object to go from:
let obj = {
  my: 'name',
  is: 'Rudolf',
  the: 'raindeer'
}
// to this:
'my name is Rudolf the raindeer'
Object.entries(obj).map(value => value.join(" ")).join(' ')
// the first join is for values[0] and [1], the second is between each line
```

---

## 152: Note: ES8 + ES9 Async Await

Note: ES9 + ES8 Async Await
**Async Await** is an advanced feature of Javascript that is better learned after you have an understanding of asynchronous javascript. We cover these topics in an upcoming video in this section titled **How Javascript Works** as well as in the **HTTP + AJAX + JSON + Asynchronous Javascript** section. The Async Await video is there since you will understand it better once you are in that part of the course. If you would like, you can jump there and watch the video now, but I recommend the video flow in the order that I have set up the course!

There is also the new ES9 (ES2018) features that have come out which require a bit more asynchronous knowledge on our part so these new feature videos will be introduced in that same section as well.

---

## 153: ES10 (ES2019)

Covers `.flat()` and `.flatMap()`

Also `trimStart()` and `trimEnd()`

`Object.fromEntries(variableName)` returns an object from arrays.

Update to `try/catch` block - no longer requires an error to be passed.

---

## 154: ES10 Exercise

```javascript
// Solve the below questions:

// #1 Turn this array into a new array: [1,2,3,[4],[5]]. Bonus if you can do it on one line
const array = [[1],[2],[3],[[[4]]],[[[5]]]]
//Solution:
console.log(array.flat(2))


// #2 Turn this array into a new array: [ 'Hello young grasshopper!', 'you are', 'learning fast!' ]
const greeting = [["Hello", "young", "grasshopper!"], ["you", "are"], ["learning", "fast!"]];
//Solution:
console.log(greeting.flatMap(x => x.join(' ')))


//#3 Turn the greeting array above into a string: 'Hello young grasshopper you are learning fast!'
//Solution
console.log(greeting.flatMap(x => x.join(' ')).join(' '))

//#4 Turn the trapped 3 number into: [3]
const trapped = [[[[[[[[[[[[[[[[[[[[[[[[[[3]]]]]]]]]]]]]]]]]]]]]]]]]];
//Solution
console.log(trapped.flat(Infinity))
// Infintiy is actually a LARGE number in JavaScipt. It represents the maximum amount of memory that we can hold for a number! Learn more here: https://riptutorial.com/javascript/example/2337/infinity-and--infinity

//#5 Clean up this email to have no whitespaces. Make the answer be in a single line (return a new string):
const userEmail3 = '     cannotfillemailformcorrectly@gmail.com   '
//Solution:
console.log(userEmail3.trimEnd().trimStart())


//#6 Turn the below users (value is their ID number) into an array: [ [ 'user1', 18273 ], [ 'user2', 92833 ], [ 'user3', 90315 ] ]
const users = { user1: 18273, user2: 92833, user3: 90315 }
//Solution
const usersArray = Object.entries(users)

//#7 change the output array of the above to have the user's IDs multiplied by 2 -- Should output:[ [ 'user1', 36546 ], [ 'user2', 185666 ], [ 'user3', 180630 ] ]
//Solution
updatedUsersArray = usersArray.map((user) => [user[0], user[1] * 2])

//#8 change the output array of question #7 back into an object with all the users IDs updated to their new version. Should output: { user1: 36546, user2: 185666, user3: 180630 }
//Solution
const updatedUsers = Object.fromEntries(updatedUsersArray)
console.log(updatedUsers)
```

---

## 155: Advanced Loops

```javascript
const basket = ['apples', 'oranges', 'grapes']
// all of these return the same thing
//1
for ( let i = 0; i < basket.length; i++) {
    console.log(basket[i]);
}

//2
basket.forEach(item => {
    console.log(item);
})

// for of
for (item of basket) {
    console.log(item);
}

// for in
const detailedBasket = {
    apples: 5,
    oranges: 10,
    grapes: 1000
}
    // properties of an object are enumerable
    // this is enumeration, not iteration
for (item in detailedBasket) {
    console.log(item);
}
// iterables = arrays, strings
```

---

## 156: Exercise: Advanced Loops

```javascript
const basket = ['apples', 'oranges', 'grapes'];
const detailedBasket = {
  apples: 5,
  oranges: 10,
  grapes: 1000
}
//1
for (let i = 0; i < basket.length; i++) {
  console.log(basket[i]);
}
//2
basket.forEach(item => {
  console.log(item);
})
for (item in detailedBasket) {
  console.log(item);
}
for (item of basket) {
  console.log(item);
}
// Question #1:
// create a function called biggestNumberInArray() that takes
// an array as a parameter and returns the biggest number.
// biggestNumberInArray([-1,0,3,100, 99, 2, 99]) should return 100;
// Use at least 3 different types of javascript loops to write this:
const array = [-1,0,3,100, 99, 2, 99] // should return 100
const array2 = ['a', 3, 4, 2] // should return 4
const array3 = [] // should return 0
function biggestNumberInArray(array) {
    highest = 0
    array.forEach(item=> {
        if (item > highest) {
            highest = item;
        }
    })
    return highest;
}

function biggestNumberInArray2(arr) {
    highest = 0
    for (item of arr) {
        if (item > highest) {
            highest = item;
        }
    }
    return highest;
}
function biggestNumberInArray3(arr) {
    let highest = 0;
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] > highest) {
        highest = arr[i];
      }
    }
    return highest;
}

// Question #2:
// Write a function checkBasket() that lets you know if the item is in the basket or not
amazonBasket = {
  glasses: 1
  books: 2
  floss: 100
}
function checkBasket(basket, lookingFor) {
    for (item in basket) {
      if (item === lookingFor) {
        return `${lookingFor} is in your basket`
      }
    }
    return 'that does not exist in your basket'
  }
```

---

## 157: Debugging

```javascript
const flattened = [[0,1], [2,3], [4,5]].reduce(
    (a, b) => a.concat(b), []);

const flattened = [[0,1], [2,3], [4,5]].reduce(
    (accumulator, array) => {
        console.log('array', array);
        console.log('accumulator', accumulator);
        return accumulator.concat(array)
}, []);

const flattened = [[0,1], [2,3], [4,5]].reduce(
    (accumulator, array) => {
        debugger;
        return accumulator.concat(array)
}, []);
```

---

## 158: How Javascript Works

- What is a program?
  - allocate memory
  - parse and execute
  
### Javascript is a single-threaded language that can be non-blocking.

- **Single-threaded** - there is only one Call Stack. The first item in is at the bottom. The last item is at the top.
  - This simplifies things.
  - Synchronous programming - executed line by line
- **Multi-threaded** languages have multiple call stacks.
- **Non-blocking** - setTimeout is a built-in function. If there is a step that might hold other things up, sending it through setTimeout causes it to get sent to setTimeout, where it will wait before sending it back for execution.
  - Call Stack => Web API => …wait => Callback Queue
    - After Call Stack is emptied, Event Loop checks the Callback Queue
  
![image](./images/s_FAECDB49EF29D21ADD9E5C332B3EBD7E7D22EDC5CFF96D9D0690BDE774508B24_1559088405552_image.png)

**Memory Heap** - Where memory is allocated

- Memory Leak - too many unused variables (especially global variables) can fill up the memory heap.

**Call Stack** - Where the code is read and executed

- Stack Overflow - when too many commands go to the call stack; such as when executing an infinite loop
  - Recursion - a function that calls itself

![image](./images/s_FAECDB49EF29D21ADD9E5C332B3EBD7E7D22EDC5CFF96D9D0690BDE774508B24_1559090128219_image.png)

---

## 159: Modules

[Modules](https://medium.com/sungthecoder/javascript-module-module-loader-module-bundler-es6-module-confused-yet-6343510e7bde)

---

## 160: Resources: Modules

[Modules](https://hacks.mozilla.org/2018/03/es-modules-a-cartoon-deep-dive/)

---

## 161: Extra Javascript Practice

1. [https://github.com/getify/You-Dont-Know-JS](https://github.com/getify/You-Dont-Know-JS)
2. [http://javascript.info/](http://javascript.info/)
3. [http://dmitrysoshnikov.com/ecmascript/javascript-the-core-2nd-edition/](http://dmitrysoshnikov.com/ecmascript/javascript-the-core-2nd-edition/)

---

## 162: Optional Exercise: Javascript Logic

[How to Think Like a Programmer - Lessons in Problem Solving](https://www.freecodecamp.org/news/how-to-think-like-a-programmer-lessons-in-problem-solving-d1d8bf1de7d2/)

Great article! As for the problems, I’m just not there yet with Javascript. I may come back to this later.

---
