# Section 24: Node.js and Express.js

## 252: Intro to Node.js

Just basic Node intro

---

## 253: Latest Version of Node.js

Done.

---

## 254: For Windows Users

Nope.

---

## 255: Running `script.js` in Node

```js
const a = 4;
const b = 5;

console.log(__dirname);

setTimeout(() =>{
  console.log(a+b)
}, 3000)

// Returns: (second line after 3 seconds)
/*
/home/steve/MEGA/Coding/Udemy-WebDev2019/Lesson Files/255
9
*/
```

---

## 256: Modules in Node

### script.js

```js
const script2 = require('./script2.js')

const a = script2.largeNumber;
const b = 5;

console.log(a+b);
```

### script2.js

```js
const largeNumber = 356;

module.exports = {
  largeNumber: largeNumber
};
```

### Node.js Command Line

```bash
node script
361
```

---

## 257: ES6 Modules in Node

Version 12.2 will allow ES6 modules (allowing `export` or `import`)

---

## 258: Types of Modules

- Used same files as in 255 above
- Installed `nodemon` using npm

### package.json

```json
{
  "name": "258",
  "version": "1.0.0",
  "description": "",
  "main": "script.js",
  "scripts": {
    "start": "nodemon" // added this line
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "nodemon": "^1.19.4"
  }
}
```

Now, when we run `npm start`, nodemon watches for changes.

### CLI

```bash
> nodemon

[nodemon] 1.19.4
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node script.js`
361
# changed value of b from 5 to 6
[nodemon] clean exit - waiting for changes before restart
[nodemon] restarting due to changes...
[nodemon] starting `node script.js`
362
[nodemon] clean exit - waiting for changes before restart

```

---

## 259: Building a Server

### server.js

```js
const http = require('http');

const server = http.createServer(() => {
    console.log('I hear you! Thanks for the request')
})

server.listen(3000);
```

- ran `node server.js` in command line. Opened `localhost:3000` in browser, then 'I hear you...' was printed in CLI.

### updated server.js

```js
const http = require('http');

const server = http.createServer((request, response) => {
    console.log('headers', request.headers)
    console.log('method', request.method)
    console.log('url', request.url)

    response.setHeader('Content-Type', 'text/html');
    response.end('<h1>Hellooooooo</h1>')
})

server.listen(3000);
```

### output

```bash
headers {
  host: 'localhost:3000',
  connection: 'keep-alive',
  'sec-fetch-mode': 'no-cors',
  'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.121 Safari/537.36 Vivaldi/2.8.1664.44',
  accept: 'image/webp,image/apng,image/*,*/*;q=0.8',
  'sec-fetch-site': 'cross-site',
  'accept-encoding': 'gzip, deflate, br',
  'accept-language': 'en-US,en;q=0.9'
}
method GET
url /favicon.ico

```

### updated server.js

```js
const http = require('http');

const server = http.createServer((request, response) => {
    // console.log('headers', request.headers)
    // console.log('method', request.method)
    // console.log('url', request.url)

    const user = {
        name: 'Steve',
        hobby: 'Running'
    }

    response.setHeader('Content-Type', 'application/json');
    response.end(JSON.stringify(user));
})

server.listen(3000);
```

### output in browser

![image](./images/image-20191030112843204.png)

---

## 260: Intro to Express.js

### server.js (updated)

```js
const express = require('express');

const app = express();

app.get('/', (req, res) => {
    res.send("getting root");
});

app.get('/profile', (req, res) => {
    res.send("getting profile");
});

app.post('/profile', (req, res) => {
    const user = {
        name: 'steve',
        hobby: 'beer'
    }
    res.send(user);
});

app.listen(3000);
```

Navigating to `localhost:3000` returns "getting root"

Navigating to `localhost:3000/profile` returns "getting profile"

---

## 261: Express Middleware

Data passes through Middleware before being displayed

### Updated server.js

```js
const express = require('express');

const app = express();

// middleware
app.use((req, res, next) => {
    console.log('<h1>Hello World</h1>')
    next();
});

app.get('/', (req, res) => {
    res.send("testtest");
});

app.listen(3000);
```

---

## 262: Express Version Update

Instead of:

```js
app.use(bodyparser.....)
```

Use this:

```js
app.use(express.urlencoded({extended: false}));
app.use(express.json());
```

---

## 263: Postman

Installed Postman

### server.js

```js
const express = require('express');

const app = express();

app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.get('/', (req, res) => {
    res.send("getting root");
});

app.get('/profile', (req, res) => {
    res.send("getting profile");
});

app.post('/profile', (req, res) => {
    console.log(req.body)
    const user = {
        name: 'steve',
        hobby: 'beer'
    }
    res.send(user);
});

app.listen(3000);
```

Used Postman to send info to server.

---

## 264: Quick Note: `req.header`

Use `req.headers` instead of `req.header`

---

## 265: RESTful APIs

- A REST API defines a set of functions which developers can perform requests and receive responses via HTTP protocol such ast GET, POST, PUT, and DELETE
  - GET - to receive a resource
  - POST - to create a resource
  - PUT - to update a resource
  - DELETE - to remove a resource

- The HTTP request is the *verb* (GET, POST, etc),and the part after the `/` is the *noun*, such as `/profile`

- Rest APIs are *stateless*, meaning each call contains all data necessary to complete itself successfully. A server doesn't need to keep or memorize things.

### server.js

```js
const express = require('express');

const app = express();

// app.use(express.urlencoded({extended: false}));
// app.use(express.json());
app.use(express.static(__dirname + '/public'))

app.listen(3000);
```

This serves up `index.html` in our `/public` folder

---

## 266: Node File System Module

```js
const fs = require('fs')

fs.readFile('./hello.txt', (err, data) => {
  if (err) {
    console.log('errrrooooorrr');
  }
  console.log('Async', data.toString());
})

const file = fs.readFileSync('./hello.txt');
console.log('Sync', file.toString());

/* Output
Sync helloooooo there!!!!
Async helloooooo there!!!!
*/

// APPEND
// fs.appendFile('./hello.txt', ' This is so cool!', err => {
//   if (err) {
//     console.log(err)
//   }
// });
/* hello.txt
helloooooo there!!!! This is so cool!
*/

// WRITE
fs.writeFile('bye.txt', 'Sad to see you go', err => {
  if (err) {
    console.log(err)
  }
})


// DELETE
fs.unlink('bye.txt', err => {
  if (err) {
    console.log(err)
  }
  console.log('Inception')
})
```

---

## 267: Exercise: Santa's Node Helper

Done

---

## 268: Exercise Resources: Santa's Node Helper

Done

---

## 269: Solution: Santa's Node Helper

### My Solutions

#### script1.js

```js
const fs = require('fs')
console.time('funchallenge');
let upStairs = 0;
let downStairs = 0;
fs.readFile('Helper.txt', (err, data) => {
  if (err) {
    console.log('errrrooooorrr');
  }
  var myData = data.toString();


  for (let i = 0; i < myData.length; i++) {
    if (myData[i] == '(') {
      upStairs++;
    } else {
      downStairs++;
    }
  }
  console.log(upStairs - downStairs);
});


console.timeEnd('funchallenge')
// 74, in 0.553s
```

#### script2.js

```js
const fs = require('fs')
console.time('funchallenge');
let upStairs = 0;
let downStairs = 0;
let position = 0;
fs.readFile('Helper.txt', (err, data) => {
  if (err) {
    console.log('errrrooooorrr');
  }
  var myData = data.toString();


  for (let i = 0; i < myData.length; i++) {
    if (myData[i] == '(') {
      upStairs++;
    } else {
      downStairs++;
    }
    position++;
    if (upStairs - downStairs == -1) {
      console.log('Position:', position);
      break;
    }
  }
  console.log(upStairs - downStairs);
});


console.timeEnd('funchallenge')
// 1795 in 0.533s
```

---
